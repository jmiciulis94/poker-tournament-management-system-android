package lt.ktu.jonmic.android.model.entities;

import java.math.BigDecimal;

public class Prize extends AbstractEntity {

    private Long id;
    private Integer placeFrom;
    private Integer placeUntil;
    private BigDecimal amount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPlaceFrom() {
        return placeFrom;
    }

    public void setPlaceFrom(Integer placeFrom) {
        this.placeFrom = placeFrom;
    }

    public Integer getPlaceUntil() {
        return placeUntil;
    }

    public void setPlaceUntil(Integer placeUntil) {
        this.placeUntil = placeUntil;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

}
