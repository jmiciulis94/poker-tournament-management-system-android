package lt.ktu.jonmic.android.activities.profile.chips;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;
import java.math.BigDecimal;
import java.util.Arrays;
import lt.ktu.jonmic.R;
import lt.ktu.jonmic.android.activities.BaseActivity;
import lt.ktu.jonmic.android.activities.tournaments.EditTournamentActivity;
import lt.ktu.jonmic.android.model.common.TaskResult;
import lt.ktu.jonmic.android.model.constants.Color;
import lt.ktu.jonmic.android.model.entities.Chip;
import lt.ktu.jonmic.android.model.entities.PokerChipsSet;
import lt.ktu.jonmic.android.tasks.HttpCallTask;
import lt.ktu.jonmic.android.utilities.MarshallingUtils;

public class EditPokerChipsSetActivity extends BaseActivity {

    public static final String EXTRA_POKER_CHIPS_SET_ID = "lt.ktu.jonmic.android.activities.profile.chips.EditPokerChipsSetActivity.POKER_CHIPS_SET_ID";

    private EditText editTextCaption;
    private EditText editTextNumberOfColors;
    private TableLayout tableLayoutChips;
    private Button buttonSave;
    private Long pokerChipsSetId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.poker_chips_set_edit);

        editTextCaption = (EditText) findViewById(R.id.pokerChipsSetEditEditTextCaption);
        editTextNumberOfColors = (EditText) findViewById(R.id.pokerChipsSetEditEditTextNumberOfColors);
        tableLayoutChips = (TableLayout) findViewById(R.id.pokerChipsSetEditTableLayoutChips);
        buttonSave = (Button) findViewById(R.id.pokerChipsSetEdittButtonSave);

        editTextNumberOfColors.addTextChangedListener(new EditTextNumberOfColorsListener());
        buttonSave.setOnClickListener(new ButtonSaveListener());

        if (getIntent().hasExtra(EditPokerChipsSetActivity.EXTRA_POKER_CHIPS_SET_ID)) {
            pokerChipsSetId = getIntent().getExtras().getLong(EditPokerChipsSetActivity.EXTRA_POKER_CHIPS_SET_ID);
            new GetPokerChipsSetTask().execute();
        }
    }

    class EditTextNumberOfColorsListener implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            String numberOfColorsText = editTextNumberOfColors.getText().toString();
            int numberOfColors = !numberOfColorsText.isEmpty() ? Integer.valueOf(numberOfColorsText) : 0;

            if (numberOfColors > tableLayoutChips.getChildCount() - 1) {
                for (int i = tableLayoutChips.getChildCount() - 1; i < numberOfColors; i++) {
                    LayoutInflater inflater = (LayoutInflater) EditPokerChipsSetActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    TableRow row = (TableRow) inflater.inflate(R.layout.poker_chips_set_edit_row, null);
                    ArrayAdapter<String> spinnerColorDataAdapter = new ArrayAdapter<>(EditPokerChipsSetActivity.this, android.R.layout.simple_spinner_item, Color.getColorNames(EditPokerChipsSetActivity.this));
                    ((Spinner) row.findViewById(R.id.pokerChipsSetEditRowSpinnerColor)).setAdapter(spinnerColorDataAdapter);
                    tableLayoutChips.addView(row);
                }
            } else {
                for (int i = tableLayoutChips.getChildCount() - 1; i > numberOfColors; i--) {
                    tableLayoutChips.removeViewAt(i);
                }
            }
        }

    }

    class ButtonSaveListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            PokerChipsSet pokerChipsSet = new PokerChipsSet();

            String caption = editTextCaption.getText().toString();

            pokerChipsSet.setId(pokerChipsSetId);
            pokerChipsSet.setCaption(caption);

            for (int i = 1; i < tableLayoutChips.getChildCount(); i++) {
                TableRow tableRow = (TableRow) tableLayoutChips.getChildAt(i);
                Spinner spinnerColor = (Spinner) tableRow.findViewById(R.id.pokerChipsSetEditRowSpinnerColor);
                String value = ((EditText) tableRow.findViewById(R.id.pokerChipsSetEditRowEditTextValue)).getText().toString();

                Chip chip = new Chip();
                chip.setColor(Color.getColorByName(EditPokerChipsSetActivity.this, (String) spinnerColor.getSelectedItem()));
                chip.setValue(!value.isEmpty() ? new BigDecimal(value) : null);

                pokerChipsSet.getChips().add(chip);
            }

            String json = MarshallingUtils.convertObjectToJsonBytes(pokerChipsSet);
            new SavePokerChipsSetTask(json).execute();
        }

    }

    class GetPokerChipsSetTask extends HttpCallTask<PokerChipsSet> {

        public GetPokerChipsSetTask() {
            super(EditPokerChipsSetActivity.this, "/poker-chips-set/" + pokerChipsSetId, GET, FORM_URLENCODED, JSON, null, PokerChipsSet.class, false, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<PokerChipsSet> result) {
            PokerChipsSet pokerChipsSet = result.getResult();

            editTextCaption.setText(pokerChipsSet.getCaption());
            for (int i = 0; i < pokerChipsSet.getChips().size(); i++) {
                Chip chip = pokerChipsSet.getChips().get(i);

                LayoutInflater inflater = (LayoutInflater) EditPokerChipsSetActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                TableRow row = (TableRow) inflater.inflate(R.layout.poker_chips_set_edit_row, null);

                ArrayAdapter<String> spinnerColorDataAdapter = new ArrayAdapter<>(EditPokerChipsSetActivity.this, android.R.layout.simple_spinner_item, Color.getColorNames(EditPokerChipsSetActivity.this));
                ((Spinner) row.findViewById(R.id.pokerChipsSetEditRowSpinnerColor)).setAdapter(spinnerColorDataAdapter);

                ((Spinner) row.findViewById(R.id.pokerChipsSetEditRowSpinnerColor)).setSelection(Arrays.asList(Color.values()).indexOf(chip.getColor()));
                ((EditText) row.findViewById(R.id.pokerChipsSetEditRowEditTextValue)).setText(chip.getValue().stripTrailingZeros().toPlainString());

                tableLayoutChips.addView(row);
            }

            editTextNumberOfColors.setText(String.valueOf(pokerChipsSet.getChips().size()));
        }

    }

    class SavePokerChipsSetTask extends HttpCallTask<PokerChipsSet> {


        public SavePokerChipsSetTask(String data) {
            super(EditPokerChipsSetActivity.this, "/poker-chips-set", PUT, JSON, JSON, data, PokerChipsSet.class, false, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<PokerChipsSet> result) {
            if (result.getError() == null) {
                Toast.makeText(EditPokerChipsSetActivity.this, R.string.pokerChipsSet_pokerChipsSetSuccessfullySaved, Toast.LENGTH_LONG).show();

                Intent intent = new Intent();
                intent.putExtra(EditTournamentActivity.EXTRA_POKER_CHIPS_SET, result.getResult());
                setResult(Activity.RESULT_OK, intent);

                finish();
            }
        }

    }

}
