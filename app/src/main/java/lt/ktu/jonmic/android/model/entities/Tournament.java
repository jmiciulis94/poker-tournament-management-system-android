package lt.ktu.jonmic.android.model.entities;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Tournament extends AbstractEntity {

    private Long id;
    private String caption;
    private String password;
    private Integer levelTime;
    private BigDecimal buyIn;
    private BigDecimal reBuy;
    private BigDecimal addOn;
    private Integer addOnLevel;
    private BigDecimal chipsAtBeginning;
    private BigDecimal chipsForReBuy;
    private BigDecimal chipsForAddOn;
    private Blinds blinds;
    private Pause pause;
    private PokerChipsSet pokerChipsSet;
    private PrizePool prizePool;
    private List<Participant> participants = new ArrayList<>();
    private List<TournamentAction> tournamentActions = new ArrayList<>();
    private Boolean deleted;
    private Integer version;
    private Long userId;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getLevelTime() {
        return levelTime;
    }

    public void setLevelTime(Integer levelTime) {
        this.levelTime = levelTime;
    }

    public BigDecimal getBuyIn() {
        return buyIn;
    }

    public void setBuyIn(BigDecimal buyIn) {
        this.buyIn = buyIn;
    }

    public BigDecimal getReBuy() {
        return reBuy;
    }

    public void setReBuy(BigDecimal reBuy) {
        this.reBuy = reBuy;
    }

    public BigDecimal getAddOn() {
        return addOn;
    }

    public void setAddOn(BigDecimal addOn) {
        this.addOn = addOn;
    }

    public Integer getAddOnLevel() {
        return addOnLevel;
    }

    public void setAddOnLevel(Integer addOnLevel) {
        this.addOnLevel = addOnLevel;
    }

    public BigDecimal getChipsAtBeginning() {
        return chipsAtBeginning;
    }

    public void setChipsAtBeginning(BigDecimal chipsAtBeginning) {
        this.chipsAtBeginning = chipsAtBeginning;
    }

    public BigDecimal getChipsForReBuy() {
        return chipsForReBuy;
    }

    public void setChipsForReBuy(BigDecimal chipsForReBuy) {
        this.chipsForReBuy = chipsForReBuy;
    }

    public BigDecimal getChipsForAddOn() {
        return chipsForAddOn;
    }

    public void setChipsForAddOn(BigDecimal chipsForAddOn) {
        this.chipsForAddOn = chipsForAddOn;
    }

    public Blinds getBlinds() {
        return blinds;
    }

    public void setBlinds(Blinds blinds) {
        this.blinds = blinds;
    }

    public Pause getPause() {
        return pause;
    }

    public void setPause(Pause pause) {
        this.pause = pause;
    }

    public PokerChipsSet getPokerChipsSet() {
        return pokerChipsSet;
    }

    public void setPokerChipsSet(PokerChipsSet pokerChipsSet) {
        this.pokerChipsSet = pokerChipsSet;
    }

    public PrizePool getPrizePool() {
        return prizePool;
    }

    public void setPrizePool(PrizePool prizePool) {
        this.prizePool = prizePool;
    }

    public List<Participant> getParticipants() {
        return participants;
    }

    public void setParticipants(List<Participant> participants) {
        this.participants = participants;
    }

    public List<TournamentAction> getTournamentActions() {
        return tournamentActions;
    }

    public void setTournamentActions(List<TournamentAction> tournamentActions) {
        this.tournamentActions = tournamentActions;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return caption;
    }

}
