package lt.ktu.jonmic.android.model.entities;

public class Pause extends AbstractEntity {

    private Long id;
    private String caption;
    private Integer pauseMinute;
    private Integer numberOfLevels;
    private Integer pauseLength;
    private Integer addOnPauseLength;
    private Boolean deleted;
    private Long userId;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public Integer getPauseMinute() {
        return pauseMinute;
    }

    public void setPauseMinute(Integer pauseMinute) {
        this.pauseMinute = pauseMinute;
    }

    public Integer getNumberOfLevels() {
        return numberOfLevels;
    }

    public void setNumberOfLevels(Integer numberOfLevels) {
        this.numberOfLevels = numberOfLevels;
    }

    public Integer getPauseLength() {
        return pauseLength;
    }

    public void setPauseLength(Integer pauseLength) {
        this.pauseLength = pauseLength;
    }

    public Integer getAddOnPauseLength() {
        return addOnPauseLength;
    }

    public void setAddOnPauseLength(Integer addOnPauseLength) {
        this.addOnPauseLength = addOnPauseLength;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return caption;
    }

}
