package lt.ktu.jonmic.android.activities.tournaments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

import lt.ktu.jonmic.R;
import lt.ktu.jonmic.android.activities.BaseActivity;
import lt.ktu.jonmic.android.activities.settings.SettingsActivity;
import lt.ktu.jonmic.android.activities.users.LoginActivity;
import lt.ktu.jonmic.android.gui.GUIElementCreators;
import lt.ktu.jonmic.android.model.common.TaskResult;
import lt.ktu.jonmic.android.model.constants.Action;
import lt.ktu.jonmic.android.model.constants.PrizePoolType;
import lt.ktu.jonmic.android.model.entities.Bet;
import lt.ktu.jonmic.android.model.entities.Chip;
import lt.ktu.jonmic.android.model.entities.Participant;
import lt.ktu.jonmic.android.model.entities.Prize;
import lt.ktu.jonmic.android.model.entities.Tournament;
import lt.ktu.jonmic.android.model.entities.TournamentAction;
import lt.ktu.jonmic.android.tasks.HttpCallTask;
import lt.ktu.jonmic.android.utilities.DateUtils;
import lt.ktu.jonmic.android.utilities.MarshallingUtils;
import lt.ktu.jonmic.android.utilities.TournamentUtils;

public class TournamentActivity extends BaseActivity {

    public static final String EXTRA_TOURNAMENT_ID = "lt.ktu.jonmic.android.activities.tournaments.TournamentActivity.TOURNAMENT_ID";
    public static final String EXTRA_TOURNAMENT_PASSWORD = "lt.ktu.jonmic.android.activities.tournaments.TournamentActivity.TOURNAMENT_PASSWORD";

    private TextView textViewPause;
    private TextView textViewTimeForReBuys;
    private TextView textViewCaption;
    private TextView textViewStartTime;
    private TextView textViewPrices;
    private TextView textViewLevelTime;
    private TextView textViewNumberOfParticipants;
    private TextView textViewAverageStack;
    private TextView textViewTotalReBuysAndAddOns;
    private TextView textViewTime;
    private TextView textViewBlinds;
    private TextView textViewNextLevelBlinds;
    private TextView textViewOneMinuteTime;
    private TextView textViewParticipants;
    private TextView textViewPrizePool;
    private LinearLayout linearLayoutTimes;
    private LinearLayout linearLayoutCaption;
    private LinearLayout linearLayoutInfo;
    private LinearLayout linearLayoutStatistics;
    private LinearLayout linearLayoutChips;
    private LinearLayout linearLayoutPrizePoolAndParticipants;
    private LinearLayout linearLayoutButtons;
    private Button buttonStart;
    private Button buttonFinish;
    private Button buttonPause;
    private Button buttonResume;
    private Button buttonSetLevel;
    private Button buttonParticipantDrop;
    private Button buttonParticipantReBuy;
    private Button buttonParticipantAddOn;
    private Button buttonOneMinuteStart;
    private Button buttonOneMinuteCancel;
    private Tournament tournament;
    private Long tournamentId;
    private String tournamentPassword;
    private int lastKnownLevel = -1;
    private int lastPauseHour = -1;
    private Timer timer = new Timer();
    private GetTournamentJob getTournamentJob = new GetTournamentJob();
    private Handler timerHandler = new Handler();
    private Handler tournamentHandler = new Handler();
    private String pauseTime;
    private Date oneMinuteEndTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tournament);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        textViewPause = (TextView) findViewById(R.id.tournamentTextViewPause);
        textViewTimeForReBuys = (TextView) findViewById(R.id.tournamentTextViewTimeForReBuys);
        textViewCaption = (TextView) findViewById(R.id.tournamentTextViewCaption);
        textViewStartTime = (TextView) findViewById(R.id.tournamentTextViewStartTime);
        textViewPrices = (TextView) findViewById(R.id.tournamentTextViewPrices);
        textViewLevelTime = (TextView) findViewById(R.id.tournamentTextViewLevelTime);
        textViewNumberOfParticipants = (TextView) findViewById(R.id.tournamentTextViewNumberOfParticipants);
        textViewAverageStack = (TextView) findViewById(R.id.tournamentTextViewAverageStack);
        textViewTotalReBuysAndAddOns = (TextView) findViewById(R.id.tournamentTextViewTotalReBuysAndAddOns);
        textViewTime = (TextView) findViewById(R.id.tournamentTextViewTime);
        textViewBlinds = (TextView) findViewById(R.id.tournamentTextViewBlinds);
        textViewNextLevelBlinds = (TextView) findViewById(R.id.tournamentTextViewNextLevelBlinds);
        textViewOneMinuteTime = (TextView) findViewById(R.id.tournamentTextViewOneMinuteTime);
        textViewParticipants = (TextView) findViewById(R.id.tournamentTextViewParticipants);
        textViewPrizePool = (TextView) findViewById(R.id.tournamentTextViewPrizePool);
        linearLayoutTimes = (LinearLayout) findViewById(R.id.tournamentLinearLayoutTimes);
        linearLayoutCaption = (LinearLayout) findViewById(R.id.tournamentLinearLayoutCaption);
        linearLayoutInfo = (LinearLayout) findViewById(R.id.tournamentLinearLayoutInfo);
        linearLayoutStatistics = (LinearLayout) findViewById(R.id.tournamentLinearLayoutStatistics);
        linearLayoutChips = (LinearLayout) findViewById(R.id.tournamentLinearLayoutChips);
        linearLayoutPrizePoolAndParticipants = (LinearLayout) findViewById(R.id.tournamentLinearLayoutPrizePoolAndParticipants);
        linearLayoutButtons = (LinearLayout) findViewById(R.id.tournamentLinearLayoutButtons);
        buttonStart = (Button) findViewById(R.id.tournamentButtonStart);
        buttonFinish = (Button) findViewById(R.id.tournamentButtonFinish);
        buttonPause = (Button) findViewById(R.id.tournamentButtonPause);
        buttonResume = (Button) findViewById(R.id.tournamentButtonResume);
        buttonSetLevel = (Button) findViewById(R.id.tournamentButtonSetLevel);
        buttonParticipantDrop = (Button) findViewById(R.id.tournamentButtonParticipantDrop);
        buttonParticipantReBuy = (Button) findViewById(R.id.tournamentButtonParticipantReBuy);
        buttonParticipantAddOn = (Button) findViewById(R.id.tournamentButtonParticipantAddOn);
        buttonOneMinuteStart = (Button) findViewById(R.id.tournamentButtonParticipantOneMinuteStart);
        buttonOneMinuteCancel = (Button) findViewById(R.id.tournamentButtonParticipantOneMinuteCancel);

        hideViewIfNotNeeded(textViewTimeForReBuys, SettingsActivity.showTimeForReBuys(this));
        hideViewIfNotNeeded(textViewPause, SettingsActivity.showTimeUntilPause(this));
        hideViewIfNotNeeded(linearLayoutTimes, SettingsActivity.showTimeForReBuys(this) || SettingsActivity.showTimeUntilPause(this));
        hideViewIfNotNeeded(linearLayoutCaption, SettingsActivity.showTournamentCaption(this));
        hideViewIfNotNeeded(textViewStartTime, SettingsActivity.showTournamentStartTime(this));
        hideViewIfNotNeeded(textViewPrices, SettingsActivity.showPrices(this));
        hideViewIfNotNeeded(textViewLevelTime, SettingsActivity.showLevelTime(this));
        hideViewIfNotNeeded(linearLayoutInfo, SettingsActivity.showTournamentStartTime(this) || SettingsActivity.showPrices(this) || SettingsActivity.showLevelTime(this));
        hideViewIfNotNeeded(textViewNumberOfParticipants, SettingsActivity.showNumberOfParticipants(this));
        hideViewIfNotNeeded(textViewAverageStack, SettingsActivity.showAverageStack(this));
        hideViewIfNotNeeded(textViewTotalReBuysAndAddOns, SettingsActivity.showTotalReBuysAndAddOns(this));
        hideViewIfNotNeeded(linearLayoutStatistics, SettingsActivity.showNumberOfParticipants(this) || SettingsActivity.showAverageStack(this) || SettingsActivity.showTotalReBuysAndAddOns(this));
        hideViewIfNotNeeded(linearLayoutChips, SettingsActivity.showChipValues(this));
        hideViewIfNotNeeded(textViewParticipants, SettingsActivity.showParticipants(this));
        hideViewIfNotNeeded(textViewPrizePool, SettingsActivity.showPrizePool(this));
        hideViewIfNotNeeded(linearLayoutPrizePoolAndParticipants, SettingsActivity.showParticipants(this) || SettingsActivity.showPrizePool(this));
        hideViewIfNotNeeded(linearLayoutButtons, SettingsActivity.showButtons(this));

        buttonOneMinuteCancel.setVisibility(View.GONE);
        textViewOneMinuteTime.setVisibility(View.GONE);

        buttonStart.setOnClickListener(new ButtonStartListener());
        buttonFinish.setOnClickListener(new ButtonFinishListener());
        buttonPause.setOnClickListener(new ButtonPauseListener());
        buttonResume.setOnClickListener(new ButtonResumeListener());
        buttonSetLevel.setOnClickListener(new ButtonSetLevelListener());
        buttonParticipantDrop.setOnClickListener(new ButtonParticipantDropListener());
        buttonParticipantReBuy.setOnClickListener(new ButtonParticipantReBuyListener());
        buttonParticipantAddOn.setOnClickListener(new ButtonParticipantAddOnListener());
        buttonOneMinuteStart.setOnClickListener(new ButtonOneMinuteStartListener());
        buttonOneMinuteCancel.setOnClickListener(new ButtonOneMinuteCancelListener());

        tournamentId = getIntent().getExtras().getLong(TournamentActivity.EXTRA_TOURNAMENT_ID);
        tournamentPassword = getIntent().getExtras().getString(TournamentActivity.EXTRA_TOURNAMENT_PASSWORD);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new GetTournamentTask().execute();
        timerHandler.postDelayed(timer, 1000);
        tournamentHandler.postDelayed(getTournamentJob, 2000);
    }

    @Override
    protected void onPause() {
        super.onPause();
        timerHandler.removeCallbacks(timer);
        tournamentHandler.removeCallbacks(getTournamentJob);
    }

    private void hideViewIfNotNeeded(View view, boolean show) {
        if (!show) {
            view.setVisibility(View.GONE);
        }
    }

    class ButtonStartListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            GUIElementCreators.showAlertDialog(TournamentActivity.this, getString(R.string.tournamentDashboard_startConfirmation), new ButtonConfirmStartListener());
        }

        class ButtonConfirmStartListener implements DialogInterface.OnClickListener {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                new StartTournamentTask().execute();
            }

        }

    }

    class ButtonFinishListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            GUIElementCreators.showAlertDialog(TournamentActivity.this, getString(R.string.tournamentDashboard_finishConfirmation), new ButtonConfirmFinishListener());
        }

        class ButtonConfirmFinishListener implements DialogInterface.OnClickListener {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                new FinishTournamentTask().execute();
            }

        }

    }

    class ButtonPauseListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            new PauseTournamentTask().execute();
        }

    }

    class ButtonResumeListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            new ResumeTournamentTask().execute();
        }

    }

    class ButtonSetLevelListener implements View.OnClickListener {

        int selectedIndex = 0;

        @Override
        public void onClick(View view) {
            String[] blinds = new String[tournament.getBlinds().getBets().size()];
            for (int i = 0; i < tournament.getBlinds().getBets().size(); i++) {
                blinds[i] = String.format("%s/%s", tournament.getBlinds().getBets().get(i).getSmallBlind().stripTrailingZeros().toPlainString(), tournament.getBlinds().getBets().get(i).getBigBlind().stripTrailingZeros().toPlainString());
            }

            GUIElementCreators.showDialogWithDropDownItems(TournamentActivity.this, getString(R.string.tournamentDashboard_selectBlinds), blinds, new BlindsSelectListener(), getString(R.string.common_ok), new ButtonOKListener(), getString(R.string.common_cancel));
        }

        class BlindsSelectListener implements DialogInterface.OnClickListener {

            public BlindsSelectListener() {
                selectedIndex = 0;
            }

            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectedIndex = which;
            }

        }

        class ButtonOKListener implements DialogInterface.OnClickListener {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Bet bet = tournament.getBlinds().getBets().get(selectedIndex);

                String json = MarshallingUtils.convertObjectToJsonBytes(bet.getLevel());
                new RestartFromLevelTask(json, bet).execute();
            }

        }

    }

    class ButtonParticipantDropListener implements View.OnClickListener {

        int selectedIndex = 0;

        @Override
        public void onClick(View view) {
            List<Participant> participants = TournamentUtils.getNonDroppedParticipants(tournament);
            String[] participantsNames = new String[participants.size()];
            for (int i = 0; i < participants.size(); i++) {
                participantsNames[i] = participants.get(i).toString();
            }

            if (!participants.isEmpty()) {
                GUIElementCreators.showDialogWithDropDownItems(TournamentActivity.this, getString(R.string.tournamentDashboard_selectParticipant), participantsNames, new ParticipantSelectListener(), getString(R.string.common_ok), new ButtonOKListener(), getString(R.string.common_cancel));
            } else {
                Toast.makeText(TournamentActivity.this, R.string.tournamentDashboard_allParticipantsAreDropped, Toast.LENGTH_LONG).show();
            }
        }

        class ParticipantSelectListener implements DialogInterface.OnClickListener {

            public ParticipantSelectListener() {
                selectedIndex = 0;
            }

            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectedIndex = which;
            }

        }

        class ButtonOKListener implements DialogInterface.OnClickListener {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Participant participant = TournamentUtils.getNonDroppedParticipants(tournament).get(selectedIndex);
                String json = MarshallingUtils.convertObjectToJsonBytes(participant.getId());
                new ParticipantDropTask(json, participant).execute();
            }

        }

    }

    class ButtonParticipantReBuyListener implements View.OnClickListener {

        int selectedIndex = 0;

        @Override
        public void onClick(View view) {
            List<Participant> participants = TournamentUtils.getNonDroppedParticipants(tournament);
            String[] participantsNames = new String[participants.size()];
            for (int i = 0; i < participants.size(); i++) {
                participantsNames[i] = participants.get(i).toString();
            }

            if (!participants.isEmpty()) {
                GUIElementCreators.showDialogWithDropDownItems(TournamentActivity.this, getString(R.string.tournamentDashboard_selectParticipant), participantsNames, new ParticipantSelectListener(), getString(R.string.common_ok), new ButtonOKListener(), getString(R.string.common_cancel));
            } else {
                Toast.makeText(TournamentActivity.this, R.string.tournamentDashboard_allParticipantsAreDropped, Toast.LENGTH_LONG).show();
            }
        }

        class ParticipantSelectListener implements DialogInterface.OnClickListener {

            public ParticipantSelectListener() {
                selectedIndex = 0;
            }

            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectedIndex = which;
            }

        }

        class ButtonOKListener implements DialogInterface.OnClickListener {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Participant participant = TournamentUtils.getNonDroppedParticipants(tournament).get(selectedIndex);
                String json = MarshallingUtils.convertObjectToJsonBytes(participant.getId());
                new ParticipantReBuyTask(json, participant).execute();
            }

        }

    }

    class ButtonParticipantAddOnListener implements View.OnClickListener {

        int selectedIndex = 0;

        @Override
        public void onClick(View view) {
            List<Participant> participants = TournamentUtils.getNonDroppedParticipantsThatDoesNotHaveAddOn(tournament);
            String[] participantsNames = new String[participants.size()];
            for (int i = 0; i < participants.size(); i++) {
                participantsNames[i] = participants.get(i).toString();
            }

            if (!participants.isEmpty()) {
                GUIElementCreators.showDialogWithDropDownItems(TournamentActivity.this, getString(R.string.tournamentDashboard_selectParticipant), participantsNames, new ParticipantSelectListener(), getString(R.string.common_ok), new ButtonOKListener(), getString(R.string.common_cancel));
            } else {
                Toast.makeText(TournamentActivity.this, R.string.tournamentDashboard_allParticipantsAlreadyDidAddOns, Toast.LENGTH_LONG).show();
            }
        }

        class ParticipantSelectListener implements DialogInterface.OnClickListener {

            public ParticipantSelectListener() {
                selectedIndex = 0;
            }

            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectedIndex = which;
            }

        }

        class ButtonOKListener implements DialogInterface.OnClickListener {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Participant participant = TournamentUtils.getNonDroppedParticipantsThatDoesNotHaveAddOn(tournament).get(selectedIndex);
                String json = MarshallingUtils.convertObjectToJsonBytes(participant.getId());
                new ParticipantAddOnTask(json, participant).execute();
            }

        }

    }

    class ButtonOneMinuteStartListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            oneMinuteEndTime = DateUtils.addMinutes(new Date(), 1);
            buttonOneMinuteStart.setVisibility(View.GONE);
            textViewNextLevelBlinds.setVisibility(View.GONE);
            buttonOneMinuteCancel.setVisibility(View.VISIBLE);
            textViewOneMinuteTime.setVisibility(View.VISIBLE);
        }

    }

    class ButtonOneMinuteCancelListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            oneMinuteEndTime = null;
            buttonOneMinuteStart.setVisibility(View.VISIBLE);
            textViewNextLevelBlinds.setVisibility(View.VISIBLE);
            buttonOneMinuteCancel.setVisibility(View.GONE);
            textViewOneMinuteTime.setVisibility(View.GONE);
        }

    }

    class GetTournamentTask extends HttpCallTask<Tournament> {

        public GetTournamentTask() {
            super(TournamentActivity.this, "/tournament/" + tournamentId + "?password=" + (tournamentPassword != null ? tournamentPassword : "") + "&version=" + (tournament != null ? tournament.getVersion() : ""), GET, FORM_URLENCODED, JSON, null, Tournament.class, false, false);
        }

        @Override
        protected void postExecuteActions(TaskResult<Tournament> result) {
            if (result.getResult() == null) {
                return;
            }

            tournament = result.getResult();
            BigDecimal averageStack = TournamentUtils.getAverageStack(tournament);

            String buyIn = String.format("%s: %s %s (%s %s)", getString(R.string.tournamentDashboard_buyIn), tournament.getBuyIn() != null ? tournament.getBuyIn().toPlainString() : "--", SettingsActivity.getCurrency(TournamentActivity.this), tournament.getChipsAtBeginning() != null ? tournament.getChipsAtBeginning().stripTrailingZeros().toPlainString() : "--", getString(R.string.tournamentDashboard_chipsLowercase));
            String reBuy = String.format("%s: %s %s (%s %s)", getString(R.string.tournamentDashboard_reBuy), tournament.getReBuy() != null ? tournament.getReBuy().toPlainString() : "--", SettingsActivity.getCurrency(TournamentActivity.this), tournament.getChipsForReBuy() != null ? tournament.getChipsForReBuy().stripTrailingZeros().toPlainString() : "--", getString(R.string.tournamentDashboard_chipsLowercase));
            String addOn = String.format("%s: %s %s (%s %s)", getString(R.string.tournamentDashboard_addOn), tournament.getAddOn() != null ? tournament.getAddOn().toPlainString() : "--", SettingsActivity.getCurrency(TournamentActivity.this), tournament.getChipsForAddOn() != null ? tournament.getChipsForAddOn().stripTrailingZeros().toPlainString() : "--", getString(R.string.tournamentDashboard_chipsLowercase));

            Date tournamentStartDate = TournamentUtils.getTournamentStartDate(tournament);
            int totalReBuys = TournamentUtils.countActionsOfType(tournament, Action.PARTICIPANT_REBUY);
            int totalAddOns = TournamentUtils.countActionsOfType(tournament, Action.PARTICIPANT_ADDON);

            textViewCaption.setText(tournament.getCaption());
            textViewStartTime.setText(String.format("%s: %s", getString(R.string.tournamentDashboard_started), tournamentStartDate != null ? DateUtils.formatTime(tournamentStartDate) : "--"));
            textViewPrices.setText(String.format("%s | %s | %s", buyIn, reBuy, addOn));
            textViewLevelTime.setText(String.format("%s: %s %s", getString(R.string.tournamentDashboard_levelTime), String.valueOf(tournament.getLevelTime()), getString(R.string.tournamentDashboard_minutes)));
            textViewNumberOfParticipants.setText(String.format("%s: %d %s %d", getString(R.string.tournamentDashboard_numOfParticipants), TournamentUtils.getNonDroppedParticipants(tournament).size(), getString(R.string.tournamentDashboard_ofLowercase), tournament.getParticipants().size()));
            textViewAverageStack.setText(String.format("%s: %s", getString(R.string.tournamentDashboard_averageStack), averageStack != null ? averageStack.stripTrailingZeros().toPlainString() : "--"));
            textViewTotalReBuysAndAddOns.setText(String.format("%s %s: %d, %s: %d", getString(R.string.tournamentDashboard_total), getString(R.string.tournamentDashboard_reBuysLowercase), totalReBuys, getString(R.string.tournamentDashboard_addOnsLowercase), totalAddOns));
            textViewParticipants.setText(getTextForParticipantsHeader());
            textViewParticipants.append(getTextForParticipantsThatAreNotDropped(tournament));
            textViewParticipants.append(getTextForParticipantsThatAreDropped(tournament));
            textViewPrizePool.setText(tournament.getPrizePool() != null ? getPrizes() : "");

            if (tournament.getPokerChipsSet() != null) {
                addChips();
            }

            if (tournamentStartDate == null) {
                buttonStart.setVisibility(View.VISIBLE);
                buttonFinish.setVisibility(View.GONE);
                buttonPause.setVisibility(View.GONE);
                buttonResume.setVisibility(View.GONE);
                buttonSetLevel.setVisibility(View.GONE);
                buttonParticipantDrop.setVisibility(View.GONE);
                buttonParticipantReBuy.setVisibility(View.GONE);
                buttonParticipantAddOn.setVisibility(View.GONE);
                buttonOneMinuteStart.setVisibility(View.GONE);
            } else {
                TournamentAction lastAction = TournamentUtils.getLastTournamentActionExceptIgnored(tournament, Action.PARTICIPANT_DROP, Action.PARTICIPANT_REBUY, Action.PARTICIPANT_ADDON);

                buttonStart.setVisibility(View.GONE);
                buttonFinish.setVisibility(View.VISIBLE);
                buttonSetLevel.setVisibility(View.VISIBLE);
                buttonSetLevel.setVisibility(View.VISIBLE);

                if (lastAction.getAction() == Action.PAUSE) {
                    buttonPause.setVisibility(View.GONE);
                    buttonResume.setVisibility(View.VISIBLE);
                } else {
                    buttonPause.setVisibility(View.VISIBLE);
                    buttonResume.setVisibility(View.GONE);
                }

                buttonParticipantDrop.setVisibility(!tournament.getParticipants().isEmpty() ? View.VISIBLE : View.GONE);
                buttonParticipantReBuy.setVisibility(tournament.getReBuy() != null ? View.VISIBLE : View.GONE);
                buttonParticipantAddOn.setVisibility(tournament.getAddOn() != null ? View.VISIBLE : View.GONE);
                buttonOneMinuteStart.setVisibility(oneMinuteEndTime == null ? View.VISIBLE : View.GONE);

                if (lastAction.getAction() != Action.PAUSE) {
                    pauseTime = null;
                }
            }

            SharedPreferences sharedPreferences = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE);
            Long userId = sharedPreferences.getLong(LoginActivity.EXTRA_USER_ID, 0);
            if (!tournament.getUserId().equals(userId)) {
                linearLayoutButtons.setVisibility(View.GONE);
            }
        }

        private Spannable getTextForParticipantsHeader() {
            String participantsHeaderText = String.format("%s:", getString(R.string.tournamentDashboard_participants));
            Spannable spannable = new SpannableString(participantsHeaderText);
            spannable.setSpan(new ForegroundColorSpan(textViewParticipants.getTextColors().getDefaultColor()), 0, participantsHeaderText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            return spannable;
        }

        private Spannable getTextForParticipantsThatAreNotDropped(Tournament tournament) {
            ForegroundColorSpan color = new ForegroundColorSpan(textViewParticipants.getTextColors().getDefaultColor());
            return getSpannableForParticipants(TournamentUtils.getNonDroppedParticipants(tournament), color, 1);
        }

        private Spannable getTextForParticipantsThatAreDropped(Tournament tournament) {
            int i = TournamentUtils.getNonDroppedParticipants(tournament).size() + 1;
            ForegroundColorSpan color = new ForegroundColorSpan(Color.LTGRAY);
            return getSpannableForParticipants(TournamentUtils.getDroppedParticipants(tournament), color, i);
        }

        private Spannable getSpannableForParticipants(List<Participant> participants, ForegroundColorSpan color, int i) {
            int maxNumberOfParticipantsToShow = Integer.parseInt(SettingsActivity.getMaxNumberOfParticipantsToShow(TournamentActivity.this));

            StringBuilder result = new StringBuilder();
            for (Participant participant : participants) {
                if (maxNumberOfParticipantsToShow != 0 && i > maxNumberOfParticipantsToShow) {
                    break;
                }

                result.append(String.format("\n%d. %s", i, participant.toString()));

                int numberOfRebuys = TournamentUtils.getNumberOfReBuysThatParticipantDid(tournament, participant.getId());
                boolean haveParticipantAddOn = TournamentUtils.haveParticipantAction(tournament, participant.getId(), Action.PARTICIPANT_ADDON);

                String reBuy = getString(R.string.tournamentDashboard_reBuyLowercase);
                String reBuys = getString(R.string.tournamentDashboard_reBuysLowercase);
                String addOn = getString(R.string.tournamentDashboard_addOnLowercase);

                if (numberOfRebuys > 0 && haveParticipantAddOn) {
                    result.append(numberOfRebuys == 1 ? String.format(" (1 %s, %s)", reBuy, addOn) : String.format(" (%d %s, %s)", numberOfRebuys, reBuys, addOn));
                } else if (numberOfRebuys > 0 && !haveParticipantAddOn) {
                    result.append(numberOfRebuys == 1 ? String.format(" (1 %s)", reBuy) : String.format(" (%d %s)", numberOfRebuys, reBuys));
                } else if (numberOfRebuys == 0 && haveParticipantAddOn) {
                    result.append(String.format(" (%s)", addOn));
                }

                i++;
            }

            Spannable spannable = new SpannableString(result.toString());
            spannable.setSpan(color, 0, result.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            return spannable;
        }

        private String getPrizes() {
            Integer maxNumberOfPrizesToShow = Integer.parseInt(SettingsActivity.getMaxNumberOfPrizesToShow(TournamentActivity.this));
            BigDecimal totalPrizeAmount = TournamentUtils.getTotalPrizePool(tournament);

            StringBuilder result = new StringBuilder();
            if (tournament.getPrizePool().getType() == PrizePoolType.CURRENCY) {
                result.append(String.format("%s (%s %s):", getString(R.string.tournamentDashboard_prizes), totalPrizeAmount.toPlainString(), SettingsActivity.getCurrency(TournamentActivity.this)));
            } else if (tournament.getPrizePool().getType() == PrizePoolType.PERCENTAGES) {
                BigDecimal rake = tournament.getPrizePool().getRake() != null ? totalPrizeAmount.multiply(tournament.getPrizePool().getRake()).divide(new BigDecimal(100), RoundingMode.HALF_UP).setScale(2, RoundingMode.HALF_DOWN) : BigDecimal.ZERO;
                BigDecimal payablePrizeAmount = totalPrizeAmount.subtract(rake);
                result.append(String.format("%s (%s %s):", getString(R.string.tournamentDashboard_prizes), payablePrizeAmount.toPlainString(), SettingsActivity.getCurrency(TournamentActivity.this)));
            }

            int i = 1;
            for (Prize prize : tournament.getPrizePool().getPrizes()) {
                if (maxNumberOfPrizesToShow != 0 && i > maxNumberOfPrizesToShow) {
                    break;
                }

                if (prize.getPlaceFrom().compareTo(prize.getPlaceUntil()) == 0) {
                    if (tournament.getPrizePool().getType() == PrizePoolType.CURRENCY) {
                        result.append(String.format("\n%d: %s %s", prize.getPlaceFrom(), prize.getAmount().toPlainString(), SettingsActivity.getCurrency(TournamentActivity.this)));
                    } else if (tournament.getPrizePool().getType() == PrizePoolType.PERCENTAGES) {
                        BigDecimal prizeAmount = totalPrizeAmount.multiply(prize.getAmount()).divide(new BigDecimal(100), RoundingMode.HALF_UP).setScale(2, RoundingMode.HALF_DOWN);
                        result.append(String.format("\n%d: %s %s", prize.getPlaceFrom(), prizeAmount.toPlainString(), SettingsActivity.getCurrency(TournamentActivity.this)));
                    }
                } else {
                    if (tournament.getPrizePool().getType() == PrizePoolType.CURRENCY) {
                        result.append(String.format("\n%d-%d: %s %s", prize.getPlaceFrom(), prize.getPlaceUntil(), prize.getAmount().toPlainString(), SettingsActivity.getCurrency(TournamentActivity.this)));
                    } else if (tournament.getPrizePool().getType() == PrizePoolType.PERCENTAGES) {
                        BigDecimal prizeAmount = totalPrizeAmount.multiply(prize.getAmount()).divide(new BigDecimal(100), RoundingMode.HALF_UP).setScale(2, RoundingMode.HALF_DOWN);
                        result.append(String.format("\n%d-%d: %s %s", prize.getPlaceFrom(), prize.getPlaceUntil(), prizeAmount.toPlainString(), SettingsActivity.getCurrency(TournamentActivity.this)));
                    }
                }
                i++;
            }

            return result.toString();
        }

        private void addChips() {
            linearLayoutChips.removeAllViews();
            for (int i = 0; i < tournament.getPokerChipsSet().getChips().size(); i++) {
                Chip chip = tournament.getPokerChipsSet().getChips().get(i);

                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                LinearLayout column = (LinearLayout) inflater.inflate(R.layout.tournament_chip_column, linearLayoutChips, false);

                ImageView imageViewColor = (ImageView) column.findViewById(R.id.tournamentChipRowColor);
                imageViewColor.setImageResource(chip.getColor().getResourceId());

                ((TextView) column.findViewById(R.id.tournamentChipRowValue)).setText(chip.getValue().stripTrailingZeros().toPlainString());

                linearLayoutChips.addView(column);
            }

        }

    }

    class StartTournamentTask extends HttpCallTask<Boolean> {

        public StartTournamentTask() {
            super(TournamentActivity.this, "/tournament/" + tournament.getId() + "/start", POST, FORM_URLENCODED, JSON, null, Boolean.class, false, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<Boolean> result) {
            new GetTournamentTask().execute();
        }

    }

    class FinishTournamentTask extends HttpCallTask<Boolean> {

        public FinishTournamentTask() {
            super(TournamentActivity.this, "/tournament/" + tournament.getId() + "/finish", POST, FORM_URLENCODED, JSON, null, Boolean.class, false, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<Boolean> result) {
            if (result.getError() == null) {
                Toast.makeText(TournamentActivity.this, R.string.tournamentDashboard_tournamentWasFinished, Toast.LENGTH_LONG).show();
                finish();
            }
        }

    }

    class PauseTournamentTask extends HttpCallTask<Boolean> {

        public PauseTournamentTask() {
            super(TournamentActivity.this, "/tournament/" + tournament.getId() + "/pause", POST, FORM_URLENCODED, JSON, null, Boolean.class, false, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<Boolean> result) {
            if (result.getError() == null) {
                Toast.makeText(TournamentActivity.this, R.string.tournamentDashboard_tournamentWasPaused, Toast.LENGTH_LONG).show();
                buttonResume.setVisibility(View.VISIBLE);
                buttonPause.setVisibility(View.GONE);
                new GetTournamentTask().execute();
            }
        }

    }

    class ResumeTournamentTask extends HttpCallTask<Boolean> {

        public ResumeTournamentTask() {
            super(TournamentActivity.this, "/tournament/" + tournament.getId() + "/resume", POST, FORM_URLENCODED, JSON, null, Boolean.class, false, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<Boolean> result) {
            if (result.getError() == null) {
                Toast.makeText(TournamentActivity.this, R.string.tournamentDashboard_tournamentWasResumed, Toast.LENGTH_LONG).show();
                buttonResume.setVisibility(View.GONE);
                buttonPause.setVisibility(View.VISIBLE);
                new GetTournamentTask().execute();
            }
        }

    }

    class RestartFromLevelTask extends HttpCallTask<Boolean> {

        private Bet bet;

        public RestartFromLevelTask(String data, Bet bet) {
            super(TournamentActivity.this, "/tournament/" + tournament.getId() + "/restart-from-level", POST, JSON, JSON, data, Boolean.class, false, true);
            this.bet = bet;
        }

        @Override
        protected void postExecuteActions(TaskResult<Boolean> result) {
            if (result.getError() == null) {
                String bets = String.format("%s/%s", bet.getSmallBlind().stripTrailingZeros().toPlainString(), bet.getBigBlind().stripTrailingZeros().toPlainString());
                String messageText = String.format(getString(R.string.tournamentDashboard_tournamentWasRestartedFromLevelWithBlinds), bets);
                Toast.makeText(TournamentActivity.this, messageText, Toast.LENGTH_LONG).show();
                buttonResume.setVisibility(View.GONE);
                buttonPause.setVisibility(View.VISIBLE);
                new GetTournamentTask().execute();
            }
        }

    }

    class ParticipantDropTask extends HttpCallTask<Boolean> {

        private Participant participant;

        public ParticipantDropTask(String data, Participant participant) {
            super(TournamentActivity.this, "/tournament/" + tournament.getId() + "/participant/drop", POST, JSON, JSON, data, Boolean.class, false, true);
            this.participant = participant;
        }

        @Override
        protected void postExecuteActions(TaskResult<Boolean> result) {
            if (result.getError() == null) {
                String messageText = String.format(getString(R.string.tournamentDashboard_participantWasDropped), participant.toString());
                Toast.makeText(TournamentActivity.this, messageText, Toast.LENGTH_LONG).show();
                new GetTournamentTask().execute();
            }
        }

    }

    class ParticipantReBuyTask extends HttpCallTask<Boolean> {

        private Participant participant;

        public ParticipantReBuyTask(String data, Participant participant) {
            super(TournamentActivity.this, "/tournament/" + tournament.getId() + "/participant/re-buy", POST, JSON, JSON, data, Boolean.class, false, true);
            this.participant = participant;
        }

        @Override
        protected void postExecuteActions(TaskResult<Boolean> result) {
            if (result.getError() == null) {
                String messageText = String.format(getString(R.string.tournamentDashboard_participantDidReBuy), participant.toString());
                Toast.makeText(TournamentActivity.this, messageText, Toast.LENGTH_LONG).show();
                new GetTournamentTask().execute();
            }
        }

    }

    class ParticipantAddOnTask extends HttpCallTask<Boolean> {

        private Participant participant;

        public ParticipantAddOnTask(String data, Participant participant) {
            super(TournamentActivity.this, "/tournament/" + tournament.getId() + "/participant/add-on", POST, JSON, JSON, data, Boolean.class, false, true);
            this.participant = participant;
        }

        @Override
        protected void postExecuteActions(TaskResult<Boolean> result) {
            if (result.getError() == null) {
                String messageText = String.format(getString(R.string.tournamentDashboard_participantDidAddOn), participant.toString());
                Toast.makeText(TournamentActivity.this, messageText, Toast.LENGTH_LONG).show();
                new GetTournamentTask().execute();
            }
        }

    }

    class Timer implements Runnable {

        public void run() {
            if (tournament != null && TournamentUtils.getTournamentStartDate(tournament) != null) {
                updateDynamicFields();
                timerHandler.postDelayed(this, 100);
            } else {
                timerHandler.postDelayed(this, 1000);
            }
        }

        private void updateDynamicFields() {
            Date now = new Date();
            Integer currentLevel = TournamentUtils.getCurrentTournamentLevel(tournament);
            int totalNumberOfLevels = tournament.getBlinds().getBets().get(tournament.getBlinds().getBets().size() - 1).getLevel();

            if (TournamentUtils.isTournamentOnAutoPause(tournament)) {
                int currentHour = DateUtils.getHour(now);
                if (lastPauseHour != currentHour) {
                    lastPauseHour = currentHour;
                    playPauseSound();
                }

                Date pauseEnds = DateUtils.addMinutes(TournamentUtils.getLastTournamentAction(tournament, Action.PAUSE).getDate(), tournament.getPause().getPauseLength());
                textViewTime.setText(DateUtils.getTimeDifferece(now, pauseEnds));
                textViewBlinds.setText(getString(R.string.tournamentDashboard_pause));
                textViewOneMinuteTime.setText(DateUtils.getTimeDifferece(now, oneMinuteEndTime));
                textViewNextLevelBlinds.setText("");
            } else if (TournamentUtils.isTournamentOnUserPause(tournament)) {
                if (pauseTime == null) {
                    if (currentLevel <= totalNumberOfLevels) {
                        pauseTime = DateUtils.getTimeDifferece(now, TournamentUtils.getNextLevelStartTime(tournament, currentLevel, now));
                    } else {
                        pauseTime = "--:--";
                    }
                }

                textViewTime.setText(pauseTime);
                textViewBlinds.setText(getString(R.string.tournamentDashboard_paused));
                textViewOneMinuteTime.setText(DateUtils.getTimeDifferece(now, oneMinuteEndTime));
                textViewNextLevelBlinds.setText("");
            } else if (currentLevel != null) {
                if (lastKnownLevel == -1) {
                    lastKnownLevel = currentLevel;
                } else if (currentLevel != lastKnownLevel && currentLevel != 1) {
                    lastKnownLevel = currentLevel;
                    playLevelUpSound();
                }

                if (oneMinuteEndTime != null) {
                    if (oneMinuteEndTime.after(now)) {
                        textViewOneMinuteTime.setText(DateUtils.getTimeDifferece(now, oneMinuteEndTime));
                    } else {
                        oneMinuteEndTime = null;
                        buttonOneMinuteStart.setVisibility(View.VISIBLE);
                        textViewNextLevelBlinds.setVisibility(View.VISIBLE);
                        buttonOneMinuteCancel.setVisibility(View.GONE);
                        textViewOneMinuteTime.setVisibility(View.GONE);
                        playOneMinuteTomeOutSound();
                    }
                }

                if (tournament.getAddOnLevel() != null) {
                    if (tournament.getAddOnLevel().compareTo(currentLevel) > 0) {
                        int fullLevelsForReBuy = tournament.getAddOnLevel() - currentLevel - 1;
                        Date reBuysEndingTime = DateUtils.addMinutes(TournamentUtils.getNextLevelStartTime(tournament, currentLevel, now), fullLevelsForReBuy * tournament.getLevelTime());
                        String timeForReBuy = DateUtils.getTimeDifferece(now, reBuysEndingTime);
                        textViewTimeForReBuys.setText(String.format("%s: %s", getString(R.string.tournamentDashboard_timeForReBuy), timeForReBuy));
                    } else {
                        textViewTimeForReBuys.setText(getString(R.string.tournamentDashboard_reBuysAreNotAvailable));
                    }
                } else {
                    textViewTimeForReBuys.setText("");
                }

                if (tournament.getPause() != null) {
                    textViewPause.setText(String.format("%s: %s (%d %s)", getString(R.string.tournamentDashboard_nextPause), TournamentUtils.getTimeUntilNextPause(tournament, now), tournament.getPause().getPauseLength(), getString(R.string.tournamentDashboard_minutes)));
                }

                if (currentLevel <= totalNumberOfLevels) {
                    String bigBlind = TournamentUtils.getBigBlindOfLevel(tournament, currentLevel);
                    String smallBlind = TournamentUtils.getSmallBlindOfLevel(tournament, currentLevel);
                    String ante = TournamentUtils.getAnteOfLevel(tournament, currentLevel);

                    textViewTime.setText(DateUtils.getTimeDifferece(now, TournamentUtils.getNextLevelStartTime(tournament, currentLevel, now)));
                    if (ante != null) {
                        textViewBlinds.setText(String.format("%s %d: %s/%s, %s: %s", getString(R.string.tournamentDashboard_level), currentLevel, smallBlind, bigBlind, getString(R.string.tournamentDashboard_ante), ante));
                    } else {
                        textViewBlinds.setText(String.format("%s %d: %s/%s", getString(R.string.tournamentDashboard_level), currentLevel, smallBlind, bigBlind));
                    }
                } else {
                    textViewTime.setText("--:--");
                    textViewBlinds.setText("");
                }

                if (currentLevel < totalNumberOfLevels) {
                    String bigBlind = TournamentUtils.getBigBlindOfLevel(tournament, currentLevel + 1);
                    String smallBlind = TournamentUtils.getSmallBlindOfLevel(tournament, currentLevel + 1);
                    String ante = TournamentUtils.getAnteOfLevel(tournament, currentLevel + 1);

                    if (ante != null) {
                        textViewNextLevelBlinds.setText(String.format("(%s %d: %s/%s, %s: %s)", getString(R.string.tournamentDashboard_level), currentLevel + 1, smallBlind, bigBlind, getString(R.string.tournamentDashboard_ante), ante));
                    } else {
                        textViewNextLevelBlinds.setText(String.format("(%s %d: %s/%s)", getString(R.string.tournamentDashboard_level), currentLevel + 1, smallBlind, bigBlind));
                    }
                } else {
                    textViewNextLevelBlinds.setText("");
                }
            }
        }

        private void playLevelUpSound() {
            AudioAttributes audioAttributes = new AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_NOTIFICATION).setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION).build();
            SoundPool soundPool = new SoundPool.Builder().setAudioAttributes(audioAttributes).build();
            soundPool.setOnLoadCompleteListener(new SoundLoadCompleteListener());
            soundPool.load(TournamentActivity.this, R.raw.level_up, 1);
        }

        private void playOneMinuteTomeOutSound() {
            AudioAttributes audioAttributes = new AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_NOTIFICATION).setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION).build();
            SoundPool soundPool = new SoundPool.Builder().setAudioAttributes(audioAttributes).build();
            soundPool.setOnLoadCompleteListener(new SoundLoadCompleteListener());
            soundPool.load(TournamentActivity.this, R.raw.level_up, 1);
        }

        private void playPauseSound() {
            AudioAttributes audioAttributes = new AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_NOTIFICATION).setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION).build();
            SoundPool soundPool = new SoundPool.Builder().setAudioAttributes(audioAttributes).build();
            soundPool.setOnLoadCompleteListener(new SoundLoadCompleteListener());
            soundPool.load(TournamentActivity.this, R.raw.pause, 1);
        }

        class SoundLoadCompleteListener implements SoundPool.OnLoadCompleteListener {

            @Override
            public void onLoadComplete(SoundPool soundPool, int soundId, int status) {
                soundPool.play(soundId, 1, 1, 1, 0, 1);
            }

        }

    }

    class GetTournamentJob implements Runnable {

        public void run() {
            new GetTournamentTask().execute();
            tournamentHandler.postDelayed(this, 2000);
        }

    }

}
