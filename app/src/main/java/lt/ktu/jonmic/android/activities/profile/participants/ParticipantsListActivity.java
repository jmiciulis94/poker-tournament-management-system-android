package lt.ktu.jonmic.android.activities.profile.participants;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import java.util.List;
import lt.ktu.jonmic.R;
import lt.ktu.jonmic.android.activities.BaseActivity;
import lt.ktu.jonmic.android.gui.GUIElementCreators;
import lt.ktu.jonmic.android.model.common.TaskResult;
import lt.ktu.jonmic.android.model.entities.Participant;
import lt.ktu.jonmic.android.tasks.HttpCallTask;

public class ParticipantsListActivity extends BaseActivity {

    private ListView listViewParticipants;
    private Button buttonNew;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.participants_list);

        listViewParticipants = (ListView) findViewById(R.id.participantsListListViewParticipants);
        buttonNew = (Button) findViewById(R.id.participantsListButtonNew);

        listViewParticipants.setOnItemClickListener(new ListViewParticipantsListener());
        buttonNew.setOnClickListener(new ButtonNewListener());

        registerForContextMenu(listViewParticipants);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, view, menuInfo);

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        Participant participant = (Participant) ((ListView) view).getAdapter().getItem(info.position);
        menu.setHeaderTitle(participant.toString());

        menu.add(Menu.NONE, participant.getId().intValue(), 1, getString(R.string.common_view));
        menu.add(Menu.NONE, participant.getId().intValue(), 2, getString(R.string.common_edit));
        menu.add(Menu.NONE, participant.getId().intValue(), 3, getString(R.string.common_delete));
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getTitle().equals(getString(R.string.common_view))) {
            Intent intent = new Intent(this, ViewParticipantActivity.class);
            intent.putExtra(ViewParticipantActivity.EXTRA_PARTICIPANT_ID, (long) item.getItemId());
            startActivity(intent);
        } else if (item.getTitle().equals(getString(R.string.common_edit))) {
            Intent intent = new Intent(this, EditParticipantActivity.class);
            intent.putExtra(EditParticipantActivity.EXTRA_PARTICIPANT_ID, (long) item.getItemId());
            startActivity(intent);
        } else if (item.getTitle().equals(getString(R.string.common_delete))) {
            GUIElementCreators.showAlertDialog(this, getString(R.string.common_deleteConfirmation), new ButtonConfirmDeleteListener((long) item.getItemId()));
        }

        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        new GetParticipantsTask().execute();
    }

    class ListViewParticipantsListener implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Participant participant = (Participant) parent.getItemAtPosition(position);
            Intent intent = new Intent(ParticipantsListActivity.this, ViewParticipantActivity.class);
            intent.putExtra(ViewParticipantActivity.EXTRA_PARTICIPANT_ID, participant.getId());
            startActivity(intent);
        }

    }

    class ButtonConfirmDeleteListener implements DialogInterface.OnClickListener {

        private Long participantId;

        public ButtonConfirmDeleteListener(Long participantId) {
            this.participantId = participantId;
        }

        @Override
        public void onClick(DialogInterface dialog, int which) {
            new DeleteParticipantTask(participantId).execute();
            new GetParticipantsTask().execute();
        }

    }

    class ButtonNewListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            startActivity(new Intent(ParticipantsListActivity.this, EditParticipantActivity.class));
        }

    }

    class GetParticipantsTask extends HttpCallTask<List<Participant>> {

        public GetParticipantsTask() {
            super(ParticipantsListActivity.this, "/participant", GET, FORM_URLENCODED, JSON, null, Participant.class, true, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<List<Participant>> result) {
            listViewParticipants.setAdapter(new ArrayAdapter<>(ParticipantsListActivity.this, android.R.layout.simple_list_item_1, result.getResult()));
        }

    }

    class DeleteParticipantTask extends HttpCallTask<Boolean> {

        public DeleteParticipantTask(long participantId) {
            super(ParticipantsListActivity.this, "/participant/" + participantId, DELETE, FORM_URLENCODED, JSON, null, Boolean.class, false, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<Boolean> result) {
            if (result.getError() == null) {
                Toast.makeText(ParticipantsListActivity.this, R.string.participant_participantSuccessfullyDeleted, Toast.LENGTH_LONG).show();
            }
        }

    }

}
