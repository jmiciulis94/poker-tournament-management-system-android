package lt.ktu.jonmic.android.activities.tournaments;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import java.math.BigDecimal;
import java.util.List;
import lt.ktu.jonmic.R;
import lt.ktu.jonmic.android.activities.BaseActivity;
import lt.ktu.jonmic.android.activities.profile.blinds.EditBlindsActivity;
import lt.ktu.jonmic.android.activities.profile.chips.EditPokerChipsSetActivity;
import lt.ktu.jonmic.android.activities.profile.pauses.EditPauseActivity;
import lt.ktu.jonmic.android.activities.profile.prizes.EditPrizePoolActivity;
import lt.ktu.jonmic.android.gui.MultiSelectSpinner;
import lt.ktu.jonmic.android.model.common.TaskResult;
import lt.ktu.jonmic.android.model.entities.Blinds;
import lt.ktu.jonmic.android.model.entities.Participant;
import lt.ktu.jonmic.android.model.entities.Pause;
import lt.ktu.jonmic.android.model.entities.PokerChipsSet;
import lt.ktu.jonmic.android.model.entities.PrizePool;
import lt.ktu.jonmic.android.model.entities.Tournament;
import lt.ktu.jonmic.android.tasks.HttpCallTask;
import lt.ktu.jonmic.android.utilities.MarshallingUtils;

public class EditTournamentActivity extends BaseActivity {

    public static final String EXTRA_TOURNAMENT_ID = "lt.ktu.jonmic.android.activities.tournaments.EditTournamentActivity.TOURNAMENT_ID";
    public static final String EXTRA_BLINDS = "lt.ktu.jonmic.android.activities.tournaments.EditTournamentActivity.BLINDS";
    public static final String EXTRA_PAUSE = "lt.ktu.jonmic.android.activities.tournaments.EditTournamentActivity.PAUSE";
    public static final String EXTRA_POKER_CHIPS_SET = "lt.ktu.jonmic.android.activities.tournaments.EditTournamentActivity.POKER_CHIPS_SET";
    public static final String EXTRA_PRIZE_POOL = "lt.ktu.jonmic.android.activities.tournaments.EditTournamentActivity.PRIZE_POOL";

    private static final long EMPTY_ITEM_ID = 0L;
    private static final long NEW_ITEM_ID = -1L;

    private static final int CREATE_BLINDS_REQUEST_CODE = 1;
    private static final int CREATE_PAUSE_REQUEST_CODE = 2;
    private static final int CREATE_POKER_CHIPS_SET_REQUEST_CODE = 3;
    private static final int CREATE_PRIZE_POOL_REQUEST_CODE = 4;

    private EditText editTextCaption;
    private EditText editTextPassword;
    private EditText editTextLevelTime;
    private EditText editTextBuyIn;
    private EditText editTextReBuy;
    private EditText editTextAddOn;
    private EditText editTextAddOnLevel;
    private EditText editTextChipsAtBeginning;
    private EditText editTextChipsForReBuy;
    private EditText editTextChipsForAddOn;
    private Spinner spinnerBlinds;
    private Spinner spinnerPause;
    private Spinner spinnerPokerChipsSet;
    private Spinner spinnerPrizePool;
    private MultiSelectSpinner multiSelectSpinnerParticipants;
    private Button buttonSave;
    private List<Blinds> blinds;
    private List<Pause> pauses;
    private List<PokerChipsSet> pokerChipsSets;
    private List<PrizePool> prizePools;
    private List<Participant> participants;
    private Tournament tournament;
    private Long tournamentId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tournament_edit);

        editTextCaption = (EditText) findViewById(R.id.tournamentEditEditTextCaption);
        editTextPassword = (EditText) findViewById(R.id.tournamentEditEditTextPassword);
        editTextLevelTime = (EditText) findViewById(R.id.tournamentEditEditTextLevelTime);
        editTextBuyIn = (EditText) findViewById(R.id.tournamentEditEditTextBuyIn);
        editTextReBuy = (EditText) findViewById(R.id.tournamentEditEditTextReBuy);
        editTextAddOn = (EditText) findViewById(R.id.tournamentEditEditTextAddOn);
        editTextAddOnLevel = (EditText) findViewById(R.id.tournamentEditEditTextAddOnLevel);
        editTextChipsAtBeginning = (EditText) findViewById(R.id.tournamentEditEditTextChipsAtBeginning);
        editTextChipsForReBuy = (EditText) findViewById(R.id.tournamentEditEditTextChipsForReBuy);
        editTextChipsForAddOn = (EditText) findViewById(R.id.tournamentEditEditTextChipsForAddOn);
        spinnerBlinds = (Spinner) findViewById(R.id.tournamentEditSpinnerBlinds);
        spinnerPause = (Spinner) findViewById(R.id.tournamentEditSpinnerPause);
        spinnerPokerChipsSet = (Spinner) findViewById(R.id.tournamentEditSpinnerPokerChipsSet);
        spinnerPrizePool = (Spinner) findViewById(R.id.tournamentEditSpinnerPrizePool);
        multiSelectSpinnerParticipants = (MultiSelectSpinner) findViewById(R.id.tournamentEditMultiSelectSpinnerParticipants);
        buttonSave = (Button) findViewById(R.id.tournamentEdittButtonSave);

        buttonSave.setOnClickListener(new ButtonSaveListener());
        spinnerBlinds.setOnItemSelectedListener(new SpinnerBlindsListener());
        spinnerPause.setOnItemSelectedListener(new SpinnerPauseListener());
        spinnerPokerChipsSet.setOnItemSelectedListener(new SpinnerPokerChipsSetListener());
        spinnerPrizePool.setOnItemSelectedListener(new SpinnerPrizePoolListener());

        new GetBlindsTask(null).execute();
        new GetPausesTask(null).execute();
        new GetPokerChipsSetsTask(null).execute();
        new GetPrizePoolsTask(null).execute();
        new GetParticipantsTask().execute();

        if (getIntent().hasExtra(EditTournamentActivity.EXTRA_TOURNAMENT_ID)) {
            tournamentId = getIntent().getExtras().getLong(EditTournamentActivity.EXTRA_TOURNAMENT_ID);
            new GetTournamentTask().execute();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CREATE_BLINDS_REQUEST_CODE:
                    Blinds blinds = (Blinds) data.getSerializableExtra(EditTournamentActivity.EXTRA_BLINDS);
                    new GetBlindsTask(blinds).execute();
                    break;
                case CREATE_PAUSE_REQUEST_CODE:
                    Pause pause = (Pause) data.getSerializableExtra(EditTournamentActivity.EXTRA_PAUSE);
                    new GetPausesTask(pause).execute();
                    break;
                case CREATE_POKER_CHIPS_SET_REQUEST_CODE:
                    PokerChipsSet pokerChipsSet = (PokerChipsSet) data.getSerializableExtra(EditTournamentActivity.EXTRA_POKER_CHIPS_SET);
                    new GetPokerChipsSetsTask(pokerChipsSet).execute();
                    break;
                case CREATE_PRIZE_POOL_REQUEST_CODE:
                    PrizePool prizePool = (PrizePool) data.getSerializableExtra(EditTournamentActivity.EXTRA_PRIZE_POOL);
                    new GetPrizePoolsTask(prizePool).execute();
                    break;
            }
        } else if (resultCode == RESULT_CANCELED) {
            switch (requestCode) {
                case CREATE_BLINDS_REQUEST_CODE:
                    spinnerBlinds.setSelection(tournament != null ? blinds.indexOf(tournament.getBlinds()) : 0);
                    break;
                case CREATE_PAUSE_REQUEST_CODE:
                    spinnerPause.setSelection(tournament != null ? pauses.indexOf(tournament.getPause()) : 0);
                    break;
                case CREATE_POKER_CHIPS_SET_REQUEST_CODE:
                    spinnerPokerChipsSet.setSelection(tournament != null ? pokerChipsSets.indexOf(tournament.getPokerChipsSet()) : 0);
                    break;
                case CREATE_PRIZE_POOL_REQUEST_CODE:
                    spinnerPrizePool.setSelection(tournament != null ? prizePools.indexOf(tournament.getPrizePool()) : 0);
                    break;
            }
        }
    }

    class ButtonSaveListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            Tournament tournament = new Tournament();

            String caption = editTextCaption.getText().toString();
            String password = editTextPassword.getText().toString();
            String levelTime = editTextLevelTime.getText().toString();
            String buyIn = editTextBuyIn.getText().toString();
            String reBuy = editTextReBuy.getText().toString();
            String addOn = editTextAddOn.getText().toString();
            String addOnLevel = editTextAddOnLevel.getText().toString();
            String chipsAtBeginning = editTextChipsAtBeginning.getText().toString();
            String chipsForReBuy = editTextChipsForReBuy.getText().toString();
            String chipsForAddOn = editTextChipsForAddOn.getText().toString();

            tournament.setId(tournamentId);
            tournament.setCaption(caption);
            tournament.setPassword(password);
            tournament.setLevelTime(!levelTime.isEmpty() ? Integer.valueOf(levelTime) : null);
            tournament.setBuyIn(!buyIn.isEmpty() ? new BigDecimal(buyIn) : null);
            tournament.setReBuy(!reBuy.isEmpty() ? new BigDecimal(reBuy) : null);
            tournament.setAddOn(!addOn.isEmpty() ? new BigDecimal(addOn) : null);
            tournament.setAddOnLevel(!addOnLevel.isEmpty() ? Integer.valueOf(addOnLevel) : null);
            tournament.setChipsAtBeginning(!chipsAtBeginning.isEmpty() ? new BigDecimal(chipsAtBeginning) : null);
            tournament.setChipsForReBuy(!chipsForReBuy.isEmpty() ? new BigDecimal(chipsForReBuy) : null);
            tournament.setChipsForAddOn(!chipsForAddOn.isEmpty() ? new BigDecimal(chipsForAddOn) : null);
            tournament.setParticipants(multiSelectSpinnerParticipants.getSelectedItems());

            Blinds selectedBlinds = (Blinds) spinnerBlinds.getSelectedItem();
            tournament.setBlinds(selectedBlinds.getId() != EMPTY_ITEM_ID ? selectedBlinds : null);

            Pause selectedPause = (Pause) spinnerPause.getSelectedItem();
            tournament.setPause(selectedPause.getId() != EMPTY_ITEM_ID ? selectedPause : null);

            PokerChipsSet selectedPokerChipsSet = (PokerChipsSet) spinnerPokerChipsSet.getSelectedItem();
            tournament.setPokerChipsSet(selectedPokerChipsSet.getId() != EMPTY_ITEM_ID ? selectedPokerChipsSet : null);

            PrizePool selectedPrizePool = (PrizePool) spinnerPrizePool.getSelectedItem();
            tournament.setPrizePool(selectedPrizePool.getId() != EMPTY_ITEM_ID ? selectedPrizePool : null);

            String json = MarshallingUtils.convertObjectToJsonBytes(tournament);
            new SaveTournamentTask(json).execute();
        }

    }

    class SpinnerBlindsListener implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Blinds blinds = (Blinds) parent.getItemAtPosition(position);
            if (blinds.getId().equals(NEW_ITEM_ID)) {
                startActivityForResult(new Intent(EditTournamentActivity.this, EditBlindsActivity.class), CREATE_BLINDS_REQUEST_CODE);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> view) {
        }

    }

    class SpinnerPauseListener implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Pause pause = (Pause) parent.getItemAtPosition(position);
            if (pause.getId().equals(NEW_ITEM_ID)) {
                startActivityForResult(new Intent(EditTournamentActivity.this, EditPauseActivity.class), CREATE_PAUSE_REQUEST_CODE);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> view) {
        }

    }

    class SpinnerPokerChipsSetListener implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            PokerChipsSet pokerChipsSet = (PokerChipsSet) parent.getItemAtPosition(position);
            if (pokerChipsSet.getId().equals(NEW_ITEM_ID)) {
                startActivityForResult(new Intent(EditTournamentActivity.this, EditPokerChipsSetActivity.class), CREATE_POKER_CHIPS_SET_REQUEST_CODE);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> view) {
        }

    }

    class SpinnerPrizePoolListener implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            PrizePool prizePool = (PrizePool) parent.getItemAtPosition(position);
            if (prizePool.getId().equals(NEW_ITEM_ID)) {
                startActivityForResult(new Intent(EditTournamentActivity.this, EditPrizePoolActivity.class), CREATE_PRIZE_POOL_REQUEST_CODE);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> view) {
        }

    }

    class GetTournamentTask extends HttpCallTask<Tournament> {

        public GetTournamentTask() {
            super(EditTournamentActivity.this, "/tournament/" + tournamentId, GET, FORM_URLENCODED, JSON, null, Tournament.class, false, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<Tournament> result) {
            tournament = result.getResult();

            editTextCaption.setText(tournament.getCaption());
            editTextPassword.setText(tournament.getPassword());
            editTextLevelTime.setText(String.valueOf(tournament.getLevelTime()));
            editTextBuyIn.setText(tournament.getBuyIn() != null ? tournament.getBuyIn().stripTrailingZeros().toPlainString() : "");
            editTextReBuy.setText(tournament.getReBuy() != null ? tournament.getReBuy().stripTrailingZeros().toPlainString() : "");
            editTextAddOn.setText(tournament.getAddOn() != null ? tournament.getAddOn().stripTrailingZeros().toPlainString() : "");
            editTextAddOnLevel.setText(tournament.getAddOnLevel() != null ? String.valueOf(tournament.getAddOnLevel()) : "");
            editTextChipsAtBeginning.setText(tournament.getChipsAtBeginning() != null ? tournament.getChipsAtBeginning().stripTrailingZeros().toPlainString() : "");
            editTextChipsForReBuy.setText(tournament.getChipsForReBuy() != null ? tournament.getChipsForReBuy().stripTrailingZeros().toPlainString() : "");
            editTextChipsForAddOn.setText(tournament.getChipsForAddOn() != null ? tournament.getChipsForAddOn().stripTrailingZeros().toPlainString() : "");

            if (blinds != null) {
                spinnerBlinds.setSelection(blinds.indexOf(tournament.getBlinds()));
            }

            if (pauses != null) {
                spinnerPause.setSelection(pauses.indexOf(tournament.getPause()));
            }

            if (pokerChipsSets != null) {
                spinnerPokerChipsSet.setSelection(pokerChipsSets.indexOf(tournament.getPokerChipsSet()));
            }

            if (prizePools != null) {
                spinnerPrizePool.setSelection(prizePools.indexOf(tournament.getPrizePool()));
            }

            if (participants != null) {
                boolean[] selectedParticipants = new boolean[participants.size()];
                for (int i = 0; i < participants.size(); i++) {
                    selectedParticipants[i] = tournament.getParticipants().contains(participants.get(i));
                }

                multiSelectSpinnerParticipants.setSelected(selectedParticipants);
            }
        }

    }

    class GetBlindsTask extends HttpCallTask<List<Blinds>> {

        private Blinds blindsToSelect;

        public GetBlindsTask(Blinds blinds) {
            super(EditTournamentActivity.this, "/blinds", GET, FORM_URLENCODED, JSON, null, Blinds.class, true, true);
            this.blindsToSelect = blinds;
        }

        @Override
        protected void postExecuteActions(TaskResult<List<Blinds>> result) {
            blinds = result.getResult();

            Blinds empty = new Blinds();
            empty.setId(EMPTY_ITEM_ID);
            empty.setCaption(getString(R.string.common_selectOne));
            blinds.add(0, empty);

            Blinds createNew = new Blinds();
            createNew.setId(NEW_ITEM_ID);
            createNew.setCaption(getString(R.string.common_createNew));
            blinds.add(createNew);

            ArrayAdapter<Blinds> spinnerBlindsDataAdapter = new ArrayAdapter<>(EditTournamentActivity.this, android.R.layout.simple_spinner_dropdown_item, blinds);
            spinnerBlinds.setAdapter(spinnerBlindsDataAdapter);

            if (blindsToSelect != null) {
                spinnerBlinds.setSelection(blinds.indexOf(blindsToSelect));
            } else if (tournament != null) {
                spinnerBlinds.setSelection(blinds.indexOf(tournament.getBlinds()));
            }
        }

    }

    class GetPausesTask extends HttpCallTask<List<Pause>> {

        private Pause pauseToSelect;

        public GetPausesTask(Pause pause) {
            super(EditTournamentActivity.this, "/pause", GET, FORM_URLENCODED, JSON, null, Pause.class, true, true);
            this.pauseToSelect = pause;
        }

        @Override
        protected void postExecuteActions(TaskResult<List<Pause>> result) {
            pauses = result.getResult();

            Pause empty = new Pause();
            empty.setId(EMPTY_ITEM_ID);
            empty.setCaption(getString(R.string.common_selectOne));
            pauses.add(0, empty);

            Pause createNew = new Pause();
            createNew.setId(NEW_ITEM_ID);
            createNew.setCaption(getString(R.string.common_createNew));
            pauses.add(createNew);

            ArrayAdapter<Pause> spinnerPauseDataAdapter = new ArrayAdapter<>(EditTournamentActivity.this, android.R.layout.simple_spinner_dropdown_item, pauses);
            spinnerPause.setAdapter(spinnerPauseDataAdapter);

            if (pauseToSelect != null) {
                spinnerPause.setSelection(pauses.indexOf(pauseToSelect));
            } else if (tournament != null) {
                spinnerPause.setSelection(pauses.indexOf(tournament.getPause()));
            }
        }

    }

    class GetPokerChipsSetsTask extends HttpCallTask<List<PokerChipsSet>> {

        private PokerChipsSet pokerChipsSetToSelect;

        public GetPokerChipsSetsTask(PokerChipsSet pokerChipsSet) {
            super(EditTournamentActivity.this, "/poker-chips-set", GET, FORM_URLENCODED, JSON, null, PokerChipsSet.class, true, true);
            this.pokerChipsSetToSelect = pokerChipsSet;
        }

        @Override
        protected void postExecuteActions(TaskResult<List<PokerChipsSet>> result) {
            pokerChipsSets = result.getResult();

            PokerChipsSet empty = new PokerChipsSet();
            empty.setId(EMPTY_ITEM_ID);
            empty.setCaption(getString(R.string.common_selectOne));
            pokerChipsSets.add(0, empty);

            PokerChipsSet createNew = new PokerChipsSet();
            createNew.setId(NEW_ITEM_ID);
            createNew.setCaption(getString(R.string.common_createNew));
            pokerChipsSets.add(createNew);

            ArrayAdapter<PokerChipsSet> spinnerPokerChipsSetDataAdapter = new ArrayAdapter<>(EditTournamentActivity.this, android.R.layout.simple_spinner_dropdown_item, pokerChipsSets);
            spinnerPokerChipsSet.setAdapter(spinnerPokerChipsSetDataAdapter);

            if (pokerChipsSetToSelect != null) {
                spinnerPokerChipsSet.setSelection(pokerChipsSets.indexOf(pokerChipsSetToSelect));
            } else if (tournament != null) {
                spinnerPokerChipsSet.setSelection(pokerChipsSets.indexOf(tournament.getPokerChipsSet()));
            }
        }

    }

    class GetPrizePoolsTask extends HttpCallTask<List<PrizePool>> {

        private PrizePool prizePoolToSelect;

        public GetPrizePoolsTask(PrizePool prizePool) {
            super(EditTournamentActivity.this, "/prize-pool", GET, FORM_URLENCODED, JSON, null, PrizePool.class, true, true);
            this.prizePoolToSelect = prizePool;
        }

        @Override
        protected void postExecuteActions(TaskResult<List<PrizePool>> result) {
            prizePools = result.getResult();

            PrizePool empty = new PrizePool();
            empty.setId(EMPTY_ITEM_ID);
            empty.setCaption(getString(R.string.common_selectOne));
            prizePools.add(0, empty);

            PrizePool createNew = new PrizePool();
            createNew.setId(NEW_ITEM_ID);
            createNew.setCaption(getString(R.string.common_createNew));
            prizePools.add(createNew);

            ArrayAdapter<PrizePool> spinnerPrizePoolDataAdapter = new ArrayAdapter<>(EditTournamentActivity.this, android.R.layout.simple_spinner_dropdown_item, prizePools);
            spinnerPrizePool.setAdapter(spinnerPrizePoolDataAdapter);

            if (prizePoolToSelect != null) {
                spinnerPrizePool.setSelection(prizePools.indexOf(prizePoolToSelect));
            } else if (tournament != null) {
                spinnerPrizePool.setSelection(prizePools.indexOf(tournament.getPrizePool()));
            }
        }

    }

    class GetParticipantsTask extends HttpCallTask<List<Participant>> {

        public GetParticipantsTask() {
            super(EditTournamentActivity.this, "/participant", GET, FORM_URLENCODED, JSON, null, Participant.class, true, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<List<Participant>> result) {
            participants = result.getResult();
            multiSelectSpinnerParticipants.setItems(participants);
            multiSelectSpinnerParticipants.setSelected(new boolean[participants.size()]);
        }

    }

    class SaveTournamentTask extends HttpCallTask<Tournament> {


        public SaveTournamentTask(String data) {
            super(EditTournamentActivity.this, "/tournament", PUT, JSON, JSON, data, Tournament.class, false, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<Tournament> result) {
            if (result.getError() == null) {
                Toast.makeText(EditTournamentActivity.this, R.string.tournament_tournamentSuccessfullySaved, Toast.LENGTH_LONG).show();
                finish();
            }
        }

    }

}
