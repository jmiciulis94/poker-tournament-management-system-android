package lt.ktu.jonmic.android.model.constants;

import android.content.Context;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import lt.ktu.jonmic.R;

public enum Color {

    BLACK(R.drawable.black), BLUE(R.drawable.blue), BROWN(R.drawable.brown), GREEN(R.drawable.green), GREY(R.drawable.grey), ORANGE(R.drawable.orange), PINK(R.drawable.pink), PURPLE(R.drawable.purple), RED(R.drawable.red), WHITE(R.drawable.white), YELLOW(R.drawable.yellow);

    private int resourceId;

    Color(int resourceId) {
        this.resourceId = resourceId;
    }

    public static List<String> getColorNames(Context context) {
        List<String> colorNames = new ArrayList<>();
        for (Color color : Arrays.asList(Color.values())) {
            colorNames.add(context.getString(context.getResources().getIdentifier("color." + color.name().toLowerCase(Locale.getDefault()), "string", context.getPackageName())));
        }

        return colorNames;
    }

    public static String getColorName(Context context, Color color) {
        return context.getString(context.getResources().getIdentifier("color." + color.name().toLowerCase(Locale.getDefault()), "string", context.getPackageName()));
    }

    public static Color getColorByName(Context context, String colorName) {
        for (Color color : Arrays.asList(Color.values())) {
            if (context.getString(context.getResources().getIdentifier("color." + color.name().toLowerCase(Locale.getDefault()), "string", context.getPackageName())).equals(colorName)) {
                return color;
            }
        }

        return null;
    }

    public int getResourceId() {
        return resourceId;
    }

}
