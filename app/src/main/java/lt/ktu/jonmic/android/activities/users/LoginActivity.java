package lt.ktu.jonmic.android.activities.users;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import java.io.IOException;
import java.util.ArrayList;
import lt.ktu.jonmic.R;
import lt.ktu.jonmic.android.activities.BaseActivity;
import lt.ktu.jonmic.android.activities.menu.MenuActivity;
import lt.ktu.jonmic.android.activities.settings.SettingsActivity;
import lt.ktu.jonmic.android.model.common.ServerError;
import lt.ktu.jonmic.android.model.common.TaskResult;
import lt.ktu.jonmic.android.model.entities.User;
import lt.ktu.jonmic.android.tasks.HttpCallTask;
import lt.ktu.jonmic.android.utilities.HttpUtils;

public class LoginActivity extends BaseActivity {

    public static final String EXTRA_JSESSION_ID = "lt.ktu.jonmic.android.activities.users.LoginActivity.JSESSION_ID";
    public static final String EXTRA_USER_ID = "lt.ktu.jonmic.android.activities.users.LoginActivity.USER_ID";

    private EditText editTextUsername;
    private EditText editTextPassword;
    private CheckBox checkBoxRememberMe;
    private Button buttonLoginAsUser;
    private Button buttonLoginAsGuest;
    private Button buttonCreateAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        editTextUsername = (EditText) findViewById(R.id.loginEditTextUsername);
        editTextPassword = (EditText) findViewById(R.id.loginEditTextPassword);
        checkBoxRememberMe = (CheckBox) findViewById(R.id.loginCheckBoxRememberMe);
        buttonLoginAsUser = (Button) findViewById(R.id.loginButtonLoginAsUser);
        buttonLoginAsGuest = (Button) findViewById(R.id.loginButtonLoginAsGuest);
        buttonCreateAccount = (Button) findViewById(R.id.loginButtonCreateAccount);

        buttonLoginAsUser.setOnClickListener(new ButtonLoginAsUserListener());
        buttonLoginAsGuest.setOnClickListener(new ButtonLoginAsGuestListener());
        buttonCreateAccount.setOnClickListener(new ButtonCreateAccountListener());
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = getSharedPreferences("lt.ktu.jonmic", Context.MODE_PRIVATE);
        sharedPreferences.edit().remove(EXTRA_JSESSION_ID).apply();

        editTextUsername.setText(SettingsActivity.getUsername(this));
        editTextPassword.setText(SettingsActivity.getPassword(this));
        checkBoxRememberMe.setChecked(SettingsActivity.isRememberMe(this));
    }

    class ButtonLoginAsUserListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            SettingsActivity.setRememberMe(LoginActivity.this, checkBoxRememberMe.isChecked());
            if (checkBoxRememberMe.isChecked()) {
                SettingsActivity.setUsername(LoginActivity.this, editTextUsername.getText().toString());
                SettingsActivity.setPassword(LoginActivity.this, editTextPassword.getText().toString());
            } else {
                SettingsActivity.setUsername(LoginActivity.this, "");
                SettingsActivity.setPassword(LoginActivity.this, "");
            }

            String data = String.format("username=%s&password=%s", editTextUsername.getText().toString(), editTextPassword.getText().toString());
            new LoginTask(data).execute();
        }

    }

    class ButtonLoginAsGuestListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            startActivity(new Intent(LoginActivity.this, MenuActivity.class));
        }

    }

    class ButtonCreateAccountListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            startActivity(new Intent(LoginActivity.this, EditUserActivity.class));
        }

    }

    class LoginTask extends HttpCallTask<User> {

        public LoginTask(String data) {
            super(LoginActivity.this, "/login", POST, FORM_URLENCODED, JSON, data, User.class, false, true);
        }

        @Override
        protected TaskResult<User> doInBackground(String... params) {
            try {
                return HttpUtils.doHttpCall(SettingsActivity.getServerURL() + httpAddress, httpMethod, contentType, acceptType, data, resultClass, isList, null);
            } catch (SecurityException ex) {
                return new TaskResult<>(new ServerError(LoginActivity.this.getString(R.string.login_invalidUsernameOrPassword), new ArrayList<String>()));
            } catch (IOException ex) {
                return new TaskResult<>(new ServerError(LoginActivity.this.getString(R.string.common_canNotConnectToTheServer), new ArrayList<String>()));
            }
        }

        @Override
        protected void postExecuteActions(TaskResult<User> result) {
            SharedPreferences sharedPreferences = getSharedPreferences(LoginActivity.this.getPackageName(), Context.MODE_PRIVATE);
            sharedPreferences.edit().putString(EXTRA_JSESSION_ID, result.getSessionId()).apply();
            sharedPreferences.edit().putLong(EXTRA_USER_ID, result.getResult().getId()).apply();
            startActivity(new Intent(LoginActivity.this, MenuActivity.class));
        }

    }

}
