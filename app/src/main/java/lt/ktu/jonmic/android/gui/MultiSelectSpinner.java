package lt.ktu.jonmic.android.gui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.AttributeSet;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import java.util.ArrayList;
import java.util.List;
import lt.ktu.jonmic.R;

public class MultiSelectSpinner<T> extends Spinner {

    private List<T> items;
    private boolean[] checkedItems;
    private boolean[] previouslyCheckedItems;

    public MultiSelectSpinner(Context context) {
        super(context);
    }

    public MultiSelectSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MultiSelectSpinner(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean performClick() {
        String[] itemNames = new String[items.size()];
        for (int i = 0; i < items.size(); i++) {
            itemNames[i] = items.get(i).toString();
        }

        previouslyCheckedItems = checkedItems.clone();

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMultiChoiceItems(itemNames, checkedItems, new MultiChoiceClickListener());
        builder.setCancelable(false);
        builder.setPositiveButton(android.R.string.ok, new ButtonOkListener());
        builder.setNegativeButton(android.R.string.cancel, new ButtonCancelListener());
        builder.show();

        return true;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }

    public void setSelected(boolean[] selected) {
        this.checkedItems = selected;
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, new String[]{getLabel()});
        setAdapter(adapter);
    }

    public List<T> getSelectedItems() {
        List<T> selectedItems = new ArrayList<>();
        for (int i = 0; i < items.size(); i++) {
            if (checkedItems[i]) {
                selectedItems.add(items.get(i));
            }
        }

        return selectedItems;
    }

    private String getLabel() {
        int i = 0;
        for (int j = 0; j < items.size(); j++) {
            if (checkedItems[j] == true) {
                i++;
            }
        }

        return i + " " + getContext().getString(R.string.tournament_numberOfParticipants);
    }

    class ButtonOkListener implements DialogInterface.OnClickListener {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, new String[]{getLabel()}));
            dialog.cancel();
        }

    }

    class ButtonCancelListener implements DialogInterface.OnClickListener {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            checkedItems = previouslyCheckedItems.clone();
            setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, new String[]{getLabel()}));
            dialog.cancel();
        }

    }

    class MultiChoiceClickListener implements DialogInterface.OnMultiChoiceClickListener {

        @Override
        public void onClick(DialogInterface dialog, int which, boolean isChecked) {
            if (isChecked) {
                checkedItems[which] = true;
            } else {
                checkedItems[which] = false;
            }
        }

    }

}