package lt.ktu.jonmic.android.activities.profile.pauses;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import lt.ktu.jonmic.R;
import lt.ktu.jonmic.android.activities.BaseActivity;
import lt.ktu.jonmic.android.activities.tournaments.EditTournamentActivity;
import lt.ktu.jonmic.android.model.common.TaskResult;
import lt.ktu.jonmic.android.model.entities.Pause;
import lt.ktu.jonmic.android.tasks.HttpCallTask;
import lt.ktu.jonmic.android.utilities.MarshallingUtils;

public class EditPauseActivity extends BaseActivity {

    public static final String EXTRA_PAUSE_ID = "lt.ktu.jonmic.android.activities.profile.pauses.EditPauseActivity.PAUSE_ID";

    private EditText editTextCaption;
    private EditText editTextPauseMinute;
    private EditText editTextNumberOfLevels;
    private EditText editTextPauseLength;
    private EditText editTextAddOnPauseLength;
    private Button buttonSave;
    private Long pauseId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pause_edit);

        editTextCaption = (EditText) findViewById(R.id.pauseEditEditTextCaption);
        editTextPauseMinute = (EditText) findViewById(R.id.pauseEditEditTextPauseMinute);
        editTextNumberOfLevels = (EditText) findViewById(R.id.pauseEditEditTextNumberOfLevels);
        editTextPauseLength = (EditText) findViewById(R.id.pauseEditEditTextPauseLength);
        editTextAddOnPauseLength = (EditText) findViewById(R.id.pauseEditEditTextAddOnPauseLength);
        buttonSave = (Button) findViewById(R.id.pauseEditButtonSave);

        buttonSave.setOnClickListener(new ButtonSaveListener());

        if (getIntent().hasExtra(EditPauseActivity.EXTRA_PAUSE_ID)) {
            pauseId = getIntent().getExtras().getLong(EditPauseActivity.EXTRA_PAUSE_ID);
            new GetPauseTask().execute();
        }
    }

    class ButtonSaveListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            Pause pause = new Pause();

            String caption = editTextCaption.getText().toString();
            String pauseMinute = editTextPauseMinute.getText().toString();
            String numberOfLevels = editTextNumberOfLevels.getText().toString();
            String pauseLength = editTextPauseLength.getText().toString();
            String addOnPauseLength = editTextAddOnPauseLength.getText().toString();

            pause.setId(pauseId);
            pause.setCaption(caption);
            pause.setPauseMinute(!pauseMinute.isEmpty() ? Integer.valueOf(pauseMinute) : null);
            pause.setNumberOfLevels(!numberOfLevels.isEmpty() ? Integer.valueOf(numberOfLevels) : null);
            pause.setPauseLength(!pauseLength.isEmpty() ? Integer.valueOf(pauseLength) : null);
            pause.setAddOnPauseLength(!addOnPauseLength.isEmpty() ? Integer.valueOf(addOnPauseLength) : null);

            String json = MarshallingUtils.convertObjectToJsonBytes(pause);
            new SavePauseTask(json).execute();
        }

    }

    class GetPauseTask extends HttpCallTask<Pause> {

        public GetPauseTask() {
            super(EditPauseActivity.this, "/pause/" + pauseId, GET, FORM_URLENCODED, JSON, null, Pause.class, false, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<Pause> result) {
            Pause pause = result.getResult();

            editTextCaption.setText(pause.getCaption());
            editTextPauseMinute.setText(pause.getPauseMinute() != null ? String.valueOf(pause.getPauseMinute()) : "");
            editTextNumberOfLevels.setText(pause.getNumberOfLevels() != null ? String.valueOf(pause.getNumberOfLevels()) : "");
            editTextPauseLength.setText(pause.getPauseLength() != null ? String.valueOf(pause.getPauseLength()) : "");
            editTextAddOnPauseLength.setText(pause.getAddOnPauseLength() != null ? String.valueOf(pause.getAddOnPauseLength()) : "");
        }

    }

    class SavePauseTask extends HttpCallTask<Pause> {


        public SavePauseTask(String data) {
            super(EditPauseActivity.this, "/pause", PUT, JSON, JSON, data, Pause.class, false, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<Pause> result) {
            if (result.getError() == null) {
                Toast.makeText(EditPauseActivity.this, R.string.pause_pauseSuccessfullySaved, Toast.LENGTH_LONG).show();

                Intent intent = new Intent();
                intent.putExtra(EditTournamentActivity.EXTRA_PAUSE, result.getResult());
                setResult(Activity.RESULT_OK, intent);

                finish();
            }
        }

    }

}
