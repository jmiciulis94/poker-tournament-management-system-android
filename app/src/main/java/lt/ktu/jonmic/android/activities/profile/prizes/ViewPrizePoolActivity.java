package lt.ktu.jonmic.android.activities.profile.prizes;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import lt.ktu.jonmic.R;
import lt.ktu.jonmic.android.activities.BaseActivity;
import lt.ktu.jonmic.android.activities.settings.SettingsActivity;
import lt.ktu.jonmic.android.gui.GUIElementCreators;
import lt.ktu.jonmic.android.model.common.TaskResult;
import lt.ktu.jonmic.android.model.constants.PrizePoolType;
import lt.ktu.jonmic.android.model.entities.PrizePool;
import lt.ktu.jonmic.android.tasks.HttpCallTask;

public class ViewPrizePoolActivity extends BaseActivity {

    public static final String EXTRA_PRIZE_POOL_ID = "lt.ktu.jonmic.android.activities.profile.prizes.ViewPrizePoolActivity.PRIZE_POOL_ID";

    private TextView textViewCaption;
    private TextView textViewType;
    private TextView textViewRake;
    private TableLayout tableLayoutPrizes;
    private LinearLayout linearLayoutRake;
    private Button buttonEdit;
    private Button buttonDelete;
    private Long prizePoolId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.prize_pool_view);

        textViewCaption = (TextView) findViewById(R.id.prizePoolViewTextViewCaption);
        textViewType = (TextView) findViewById(R.id.prizePoolViewTextViewType);
        textViewRake = (TextView) findViewById(R.id.prizePoolViewTextViewRake);
        tableLayoutPrizes = (TableLayout) findViewById(R.id.prizePoolViewTableLayoutPrizes);
        linearLayoutRake = (LinearLayout) findViewById(R.id.prizePoolViewLinearLayoutRake);
        buttonEdit = (Button) findViewById(R.id.prizePoolViewButtonEdit);
        buttonDelete = (Button) findViewById(R.id.prizePoolViewButtonDelete);

        buttonEdit.setOnClickListener(new ButtonEditListener());
        buttonDelete.setOnClickListener(new ButtonDeleteListener());

        prizePoolId = getIntent().getExtras().getLong(ViewPrizePoolActivity.EXTRA_PRIZE_POOL_ID);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new GetPrizePoolTask().execute();
    }

    class ButtonEditListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(ViewPrizePoolActivity.this, EditPrizePoolActivity.class);
            intent.putExtra(EditPrizePoolActivity.EXTRA_PRIZE_POOL_ID, prizePoolId);
            startActivity(intent);
        }

    }

    class ButtonDeleteListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            GUIElementCreators.showAlertDialog(ViewPrizePoolActivity.this, getString(R.string.common_deleteConfirmation), new ButtonConfirmDeleteListener());
        }

        class ButtonConfirmDeleteListener implements DialogInterface.OnClickListener {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                new DeletePrizePoolIdTask().execute();
            }

        }

    }

    class GetPrizePoolTask extends HttpCallTask<PrizePool> {

        public GetPrizePoolTask() {
            super(ViewPrizePoolActivity.this, "/prize-pool/" + prizePoolId, GET, FORM_URLENCODED, JSON, null, PrizePool.class, false, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<PrizePool> result) {
            PrizePool prizePool = result.getResult();

            textViewCaption.setText(prizePool.getCaption());
            textViewType.setText(PrizePoolType.getPrizePoolTypeName(ViewPrizePoolActivity.this, prizePool.getType()));

            if (prizePool.getType() == PrizePoolType.PERCENTAGES) {
                textViewRake.setText(prizePool.getRake() != null ? prizePool.getRake().toPlainString() + " " + getString(R.string.prizePool_percent) : "");
            } else {
                linearLayoutRake.setVisibility(View.GONE);
            }

            tableLayoutPrizes.removeViews(1, tableLayoutPrizes.getChildCount() - 1);
            for (int i = 0; i < prizePool.getPrizes().size(); i++) {
                LayoutInflater inflater = (LayoutInflater) ViewPrizePoolActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                TableRow row = (TableRow) inflater.inflate(R.layout.prize_pool_view_row, null);

                String amountSuffix = "";
                if (prizePool.getType() == PrizePoolType.CURRENCY) {
                    amountSuffix = SettingsActivity.getCurrency(ViewPrizePoolActivity.this);
                } else if (prizePool.getType() == PrizePoolType.PERCENTAGES) {
                    amountSuffix = getString(R.string.prizePool_percent);
                }

                ((TextView) row.findViewById(R.id.prizePoolViewRowTextViewPlaceFrom)).setText(String.valueOf(prizePool.getPrizes().get(i).getPlaceFrom()));
                ((TextView) row.findViewById(R.id.prizePoolViewRowTextViewPlaceUntil)).setText(String.valueOf(prizePool.getPrizes().get(i).getPlaceUntil()));
                ((TextView) row.findViewById(R.id.prizePoolViewRowTextViewAmount)).setText(prizePool.getPrizes().get(i).getAmount().toPlainString() + " " + amountSuffix);

                tableLayoutPrizes.addView(row);
            }
        }

    }

    class DeletePrizePoolIdTask extends HttpCallTask<Boolean> {

        public DeletePrizePoolIdTask() {
            super(ViewPrizePoolActivity.this, "/prize-pool/" + prizePoolId, DELETE, FORM_URLENCODED, JSON, null, Boolean.class, false, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<Boolean> result) {
            if (result.getError() == null) {
                Toast.makeText(ViewPrizePoolActivity.this, R.string.prizePool_prizePoolSuccessfullyDeleted, Toast.LENGTH_LONG).show();
                finish();
            }
        }

    }

}
