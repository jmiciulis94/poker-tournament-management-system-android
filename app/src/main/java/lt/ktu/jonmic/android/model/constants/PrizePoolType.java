package lt.ktu.jonmic.android.model.constants;

import android.content.Context;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public enum PrizePoolType {

    CURRENCY, PERCENTAGES;

    public static List<String> getPrizePoolTypeNames(Context context) {
        List<String> prizePoolTypeNames = new ArrayList<>();
        for (PrizePoolType prizePoolType : Arrays.asList(PrizePoolType.values())) {
            prizePoolTypeNames.add(context.getString(context.getResources().getIdentifier("prizePoolType." + prizePoolType.name().toLowerCase(Locale.getDefault()), "string", context.getPackageName())));
        }

        return prizePoolTypeNames;
    }

    public static String getPrizePoolTypeName(Context context, PrizePoolType prizePoolType) {
        return context.getString(context.getResources().getIdentifier("prizePoolType." + prizePoolType.name().toLowerCase(Locale.getDefault()), "string", context.getPackageName()));
    }

    public static PrizePoolType getPrizePoolTypeByName(Context context, String prizePoolTypeLabel) {
        for (PrizePoolType prizePoolType : Arrays.asList(PrizePoolType.values())) {
            if (context.getString(context.getResources().getIdentifier("prizePoolType." + prizePoolType.name().toLowerCase(Locale.getDefault()), "string", context.getPackageName())).equals(prizePoolTypeLabel)) {
                return prizePoolType;
            }
        }

        return null;
    }

}
