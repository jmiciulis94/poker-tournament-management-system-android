package lt.ktu.jonmic.android.activities.profile.pauses;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import java.util.List;
import lt.ktu.jonmic.R;
import lt.ktu.jonmic.android.activities.BaseActivity;
import lt.ktu.jonmic.android.gui.GUIElementCreators;
import lt.ktu.jonmic.android.model.common.TaskResult;
import lt.ktu.jonmic.android.model.entities.Pause;
import lt.ktu.jonmic.android.tasks.HttpCallTask;

public class PausesListActivity extends BaseActivity {

    private ListView listViewPauses;
    private Button buttonNew;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pauses_list);

        listViewPauses = (ListView) findViewById(R.id.pausesListListViewPauses);
        buttonNew = (Button) findViewById(R.id.pausesListButtonNew);

        listViewPauses.setOnItemClickListener(new ListViewPausesListener());
        buttonNew.setOnClickListener(new ButtonNewListener());

        registerForContextMenu(listViewPauses);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, view, menuInfo);

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        Pause pause = (Pause) ((ListView) view).getAdapter().getItem(info.position);
        menu.setHeaderTitle(pause.toString());

        menu.add(Menu.NONE, pause.getId().intValue(), 1, getString(R.string.common_view));
        menu.add(Menu.NONE, pause.getId().intValue(), 2, getString(R.string.common_edit));
        menu.add(Menu.NONE, pause.getId().intValue(), 3, getString(R.string.common_delete));
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getTitle().equals(getString(R.string.common_view))) {
            Intent intent = new Intent(this, ViewPauseActivity.class);
            intent.putExtra(ViewPauseActivity.EXTRA_PAUSE_ID, (long) item.getItemId());
            startActivity(intent);
        } else if (item.getTitle().equals(getString(R.string.common_edit))) {
            Intent intent = new Intent(this, EditPauseActivity.class);
            intent.putExtra(EditPauseActivity.EXTRA_PAUSE_ID, (long) item.getItemId());
            startActivity(intent);
        } else if (item.getTitle().equals(getString(R.string.common_delete))) {
            GUIElementCreators.showAlertDialog(this, getString(R.string.common_deleteConfirmation), new ButtonConfirmDeleteListener((long) item.getItemId()));
        }

        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        new GetPausesTask().execute();
    }

    class ListViewPausesListener implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Pause pause = (Pause) parent.getItemAtPosition(position);
            Intent intent = new Intent(PausesListActivity.this, ViewPauseActivity.class);
            intent.putExtra(ViewPauseActivity.EXTRA_PAUSE_ID, pause.getId());
            startActivity(intent);
        }

    }

    class ButtonConfirmDeleteListener implements DialogInterface.OnClickListener {

        private Long pauseId;

        public ButtonConfirmDeleteListener(Long pauseId) {
            this.pauseId = pauseId;
        }

        @Override
        public void onClick(DialogInterface dialog, int which) {
            new DeletePauseTask(pauseId).execute();
            new GetPausesTask().execute();
        }

    }

    class ButtonNewListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            startActivity(new Intent(PausesListActivity.this, EditPauseActivity.class));
        }

    }

    class GetPausesTask extends HttpCallTask<List<Pause>> {

        public GetPausesTask() {
            super(PausesListActivity.this, "/pause", GET, FORM_URLENCODED, JSON, null, Pause.class, true, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<List<Pause>> result) {
            listViewPauses.setAdapter(new ArrayAdapter<>(PausesListActivity.this, android.R.layout.simple_list_item_1, result.getResult()));
        }

    }

    class DeletePauseTask extends HttpCallTask<Boolean> {

        public DeletePauseTask(long pauseId) {
            super(PausesListActivity.this, "/pause/" + pauseId, DELETE, FORM_URLENCODED, JSON, null, Boolean.class, false, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<Boolean> result) {
            if (result.getError() == null) {
                Toast.makeText(PausesListActivity.this, R.string.pause_pauseSuccessfullyDeleted, Toast.LENGTH_LONG).show();
            }
        }

    }

}
