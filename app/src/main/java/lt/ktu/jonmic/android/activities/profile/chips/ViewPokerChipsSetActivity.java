package lt.ktu.jonmic.android.activities.profile.chips;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import lt.ktu.jonmic.R;
import lt.ktu.jonmic.android.activities.BaseActivity;
import lt.ktu.jonmic.android.gui.GUIElementCreators;
import lt.ktu.jonmic.android.model.common.TaskResult;
import lt.ktu.jonmic.android.model.constants.Color;
import lt.ktu.jonmic.android.model.entities.PokerChipsSet;
import lt.ktu.jonmic.android.tasks.HttpCallTask;

public class ViewPokerChipsSetActivity extends BaseActivity {

    public static final String EXTRA_POKER_CHIPS_SET_ID = "lt.ktu.jonmic.android.activities.profile.chips.ViewPokerChipsSetActivity.POKER_CHIPS_SET_ID";

    private TextView textViewCaption;
    private TableLayout tableLayoutChips;
    private Button buttonEdit;
    private Button buttonDelete;
    private Long pokerChipsSetId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.poker_chips_set_view);

        textViewCaption = (TextView) findViewById(R.id.pokerChipsSetViewTextViewCaption);
        tableLayoutChips = (TableLayout) findViewById(R.id.pokerChipsSetViewTableLayoutChips);
        buttonEdit = (Button) findViewById(R.id.pokerChipsSetViewButtonEdit);
        buttonDelete = (Button) findViewById(R.id.pokerChipsSetViewButtonDelete);

        buttonEdit.setOnClickListener(new ButtonEditListener());
        buttonDelete.setOnClickListener(new ButtonDeleteListener());

        pokerChipsSetId = getIntent().getExtras().getLong(ViewPokerChipsSetActivity.EXTRA_POKER_CHIPS_SET_ID);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new GetPokerChipsSetTask().execute();
    }

    class ButtonEditListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(ViewPokerChipsSetActivity.this, EditPokerChipsSetActivity.class);
            intent.putExtra(EditPokerChipsSetActivity.EXTRA_POKER_CHIPS_SET_ID, pokerChipsSetId);
            startActivity(intent);
        }

    }

    class ButtonDeleteListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            GUIElementCreators.showAlertDialog(ViewPokerChipsSetActivity.this, getString(R.string.common_deleteConfirmation), new ButtonConfirmDeleteListener());
        }

        class ButtonConfirmDeleteListener implements DialogInterface.OnClickListener {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                new DeletePokerChipsSetTask().execute();
            }

        }

    }

    class GetPokerChipsSetTask extends HttpCallTask<PokerChipsSet> {

        public GetPokerChipsSetTask() {
            super(ViewPokerChipsSetActivity.this, "/poker-chips-set/" + pokerChipsSetId, GET, FORM_URLENCODED, JSON, null, PokerChipsSet.class, false, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<PokerChipsSet> result) {
            PokerChipsSet pokerChipsSet = result.getResult();

            textViewCaption.setText(pokerChipsSet.getCaption());
            tableLayoutChips.removeViews(1, tableLayoutChips.getChildCount() - 1);
            for (int i = 0; i < pokerChipsSet.getChips().size(); i++) {
                LayoutInflater inflater = (LayoutInflater) ViewPokerChipsSetActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                TableRow row = (TableRow) inflater.inflate(R.layout.poker_chips_set_view_row, null);

                ((TextView) row.findViewById(R.id.pokerChipsSetViewRowTextViewColor)).setText(Color.getColorName(ViewPokerChipsSetActivity.this, pokerChipsSet.getChips().get(i).getColor()));
                ((TextView) row.findViewById(R.id.pokerChipsSetViewRowTextViewValue)).setText(pokerChipsSet.getChips().get(i).getValue().stripTrailingZeros().toPlainString());

                tableLayoutChips.addView(row);
            }
        }

    }

    class DeletePokerChipsSetTask extends HttpCallTask<Boolean> {

        public DeletePokerChipsSetTask() {
            super(ViewPokerChipsSetActivity.this, "/poker-chips-set/" + pokerChipsSetId, DELETE, FORM_URLENCODED, JSON, null, Boolean.class, false, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<Boolean> result) {
            if (result.getError() == null) {
                Toast.makeText(ViewPokerChipsSetActivity.this, R.string.pokerChipsSet_pokerChipsSetSuccessfullyDeleted, Toast.LENGTH_LONG).show();
                finish();
            }
        }

    }

}
