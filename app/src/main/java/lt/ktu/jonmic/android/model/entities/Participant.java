package lt.ktu.jonmic.android.model.entities;

public class Participant extends AbstractEntity {

    private Long id;
    private String firstName;
    private String lastName;
    private String nickname;
    private Boolean deleted;
    private Long userId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(firstName != null ? firstName : "");
        stringBuilder.append(stringBuilder.length() > 0 ? " " : "");
        stringBuilder.append(lastName != null ? lastName : "");
        stringBuilder.append(stringBuilder.length() > 0 ? "" : nickname);

        return stringBuilder.toString().trim();
    }

}
