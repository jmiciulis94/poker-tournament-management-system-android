package lt.ktu.jonmic.android.model.common;

public class TaskResult<T> {

    private T result;
    private ServerError error;
    private String sessionId;

    public TaskResult(T result, String sessionId) {
        super();
        this.result = result;
        this.sessionId = sessionId;
    }

    public TaskResult(ServerError error) {
        super();
        this.error = error;
    }

    public T getResult() {
        return result;
    }

    public ServerError getError() {
        return error;
    }

    public String getSessionId() {
        return sessionId;
    }

}