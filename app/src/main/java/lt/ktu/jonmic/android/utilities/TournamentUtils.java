package lt.ktu.jonmic.android.utilities;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import lt.ktu.jonmic.android.model.constants.Action;
import lt.ktu.jonmic.android.model.constants.PrizePoolType;
import lt.ktu.jonmic.android.model.entities.Bet;
import lt.ktu.jonmic.android.model.entities.Participant;
import lt.ktu.jonmic.android.model.entities.Prize;
import lt.ktu.jonmic.android.model.entities.Tournament;
import lt.ktu.jonmic.android.model.entities.TournamentAction;

public class TournamentUtils {

    public static Date getTournamentStartDate(Tournament tournament) {
        for (TournamentAction tournamentAction : tournament.getTournamentActions()) {
            if (tournamentAction.getAction() == Action.START) {
                return tournamentAction.getDate();
            }
        }

        return null;
    }

    public static Date getTournamentFinishDate(Tournament tournament) {
        for (TournamentAction tournamentAction : tournament.getTournamentActions()) {
            if (tournamentAction.getAction() == Action.FINISH) {
                return tournamentAction.getDate();
            }
        }

        return null;
    }

    public static Integer getCurrentTournamentLevel(Tournament tournament) {
        int level = 1;
        for (TournamentAction tournamentAction : tournament.getTournamentActions()) {
            if (tournamentAction.getAction() == Action.LEVEL_UP || tournamentAction.getAction() == Action.RESTART_FROM_LEVEL) {
                level = Integer.valueOf(tournamentAction.getDetails());
            }
        }

        return level;
    }

    public static Date getNextLevelStartTime(Tournament tournament, int currentLevel, Date now) {
        int skippedLevels = 0;
        Date tournamentStartOrRestartDate = TournamentUtils.getTournamentStartDate(tournament);

        TournamentAction lastRestartFromLevelAction = getLastTournamentAction(tournament, Action.RESTART_FROM_LEVEL);
        if (lastRestartFromLevelAction != null) {
            skippedLevels = Integer.parseInt(lastRestartFromLevelAction.getDetails()) - 1;
            tournamentStartOrRestartDate = lastRestartFromLevelAction.getDate();
        }

        return DateUtils.addSeconds(DateUtils.addMinutes(tournamentStartOrRestartDate, (currentLevel - skippedLevels) * tournament.getLevelTime()), countSecondsOnPauses(tournament, now));
    }

    public static boolean isTournamentOnAutoPause(Tournament tournament) {
        TournamentAction tournamentAction = getLastTournamentAction(tournament, Action.START, Action.FINISH, Action.PAUSE, Action.RESUME, Action.RESTART_FROM_LEVEL);
        if (tournamentAction != null) {
            return tournamentAction.getAction() == Action.PAUSE && tournamentAction.getUser() == null;
        }

        return false;
    }

    public static boolean isTournamentOnUserPause(Tournament tournament) {
        TournamentAction tournamentAction = getLastTournamentAction(tournament, Action.START, Action.FINISH, Action.PAUSE, Action.RESUME, Action.RESTART_FROM_LEVEL);
        if (tournamentAction != null) {
            return tournamentAction.getAction() == Action.PAUSE && tournamentAction.getUser() != null;
        }

        return false;
    }

    public static String getTimeUntilNextPause(Tournament tournament, Date now) {
        if (tournament.getPause().getPauseMinute() != null) {
            Date pauseDate = DateUtils.addMinutes(DateUtils.getStartOfThisHour(), tournament.getPause().getPauseMinute());
            if (pauseDate.before(now)) {
                pauseDate = DateUtils.addMinutes(DateUtils.getStartOfNextHour(), tournament.getPause().getPauseMinute());
            }

            return DateUtils.getTimeDifferece(now, pauseDate);
        } else {
            int level = getCurrentTournamentLevel(tournament);
            Date pauseDate = getNextLevelStartTime(tournament, level, now);
            while (level % tournament.getPause().getNumberOfLevels() == 0) {
                pauseDate = DateUtils.addMinutes(pauseDate, tournament.getLevelTime());
                level++;
            }

            return DateUtils.getTimeDifferece(now, pauseDate);
        }
    }

    public static int countSecondsOnPauses(Tournament tournament, Date now) {
        int secondsOnPauses = 0;

        List<TournamentAction> importantActions = new ArrayList<>();
        for (TournamentAction tournamentAction : tournament.getTournamentActions()) {
            if (tournamentAction.getAction() == Action.PAUSE || tournamentAction.getAction() == Action.RESUME) {
                importantActions.add(tournamentAction);
            } else if (tournamentAction.getAction() == Action.RESTART_FROM_LEVEL) {
                importantActions.clear();
            }
        }

        TournamentAction lastAction = null;
        for (TournamentAction tournamentAction : importantActions) {
            if (tournamentAction.getAction() == Action.PAUSE) {
                lastAction = tournamentAction;
            } else if (tournamentAction.getAction() == Action.RESUME) {
                secondsOnPauses = secondsOnPauses + DateUtils.getSecondsBetween(lastAction.getDate(), tournamentAction.getDate());
                lastAction = null;
            }
        }

        if (lastAction != null) {
            secondsOnPauses = secondsOnPauses + DateUtils.getSecondsBetween(lastAction.getDate(), now);
        }

        return secondsOnPauses;
    }

    public static List<TournamentAction> getTournamentActions(Tournament tournament, Action... importantActions) {
        List<TournamentAction> importantTournamentActions = new ArrayList<>();
        for (TournamentAction tournamentAction : tournament.getTournamentActions()) {
            if (Arrays.asList(importantActions).contains(tournamentAction.getAction())) {
                importantTournamentActions.add(tournamentAction);
            }
        }

        return importantTournamentActions;
    }

    public static TournamentAction getLastTournamentAction(Tournament tournament, Action... importantActions) {
        List<TournamentAction> importantTournamentActions = new ArrayList<>();
        for (TournamentAction tournamentAction : tournament.getTournamentActions()) {
            if (Arrays.asList(importantActions).contains(tournamentAction.getAction())) {
                importantTournamentActions.add(tournamentAction);
            }
        }

        return !importantTournamentActions.isEmpty() ? importantTournamentActions.get(importantTournamentActions.size() - 1) : null;
    }

    public static TournamentAction getLastTournamentActionExceptIgnored(Tournament tournament, Action... ignoredActions) {
        for (int i = tournament.getTournamentActions().size() - 1; i >= 0; i--) {
            TournamentAction tournamentAction = tournament.getTournamentActions().get(i);
            if (!Arrays.asList(ignoredActions).contains(tournamentAction.getAction())) {
                return tournamentAction;
            }
        }

        return null;
    }

    public static Participant getParticipant(Tournament tournament, long participantId) {
        for (Participant participant : tournament.getParticipants()) {
            if (participant.getId().equals(participantId)) {
                return participant;
            }
        }

        return null;
    }

    public static List<Participant> getDroppedParticipants(Tournament tournament) {
        List<Participant> droppedParticipants = new ArrayList<>();
        for (Participant participant : tournament.getParticipants()) {
            if (haveParticipantAction(tournament, participant.getId(), Action.PARTICIPANT_DROP)) {
                droppedParticipants.add(participant);
            }
        }

        return droppedParticipants;
    }

    public static List<Participant> getNonDroppedParticipants(Tournament tournament) {
        List<Participant> nonDroppedParticipants = new ArrayList<>();
        for (Participant participant : tournament.getParticipants()) {
            if (!haveParticipantAction(tournament, participant.getId(), Action.PARTICIPANT_DROP)) {
                nonDroppedParticipants.add(participant);
            }
        }

        return nonDroppedParticipants;
    }

    public static List<Participant> getNonDroppedParticipantsThatDoesNotHaveAddOn(Tournament tournament) {
        List<Participant> nonDroppedParticipantsThatDoesNotHaveAddOn = new ArrayList<>();
        for (Participant participant : tournament.getParticipants()) {
            if (!haveParticipantAction(tournament, participant.getId(), Action.PARTICIPANT_DROP) && !haveParticipantAction(tournament, participant.getId(), Action.PARTICIPANT_ADDON)) {
                nonDroppedParticipantsThatDoesNotHaveAddOn.add(participant);
            }
        }

        return nonDroppedParticipantsThatDoesNotHaveAddOn;
    }

    public static int getNumberOfReBuysThatParticipantDid(Tournament tournament, long participantId) {
        int i = 0;
        for (TournamentAction tournamentAction : tournament.getTournamentActions()) {
            if (tournamentAction.getAction() == Action.PARTICIPANT_REBUY && tournamentAction.getDetails().equals(String.valueOf(participantId))) {
                i++;
            }
        }

        return i;
    }

    public static boolean haveParticipantAction(Tournament tournament, long participantId, Action action) {
        for (TournamentAction tournamentAction : tournament.getTournamentActions()) {
            if (tournamentAction.getAction() == action && tournamentAction.getDetails().equals(String.valueOf(participantId))) {
                return true;
            }
        }

        return false;
    }

    public static BigDecimal getAverageStack(Tournament tournament) {
        if (tournament.getParticipants().size() == 0 || tournament.getChipsAtBeginning() == null) {
            return null;
        }

        BigDecimal totalStack = tournament.getChipsAtBeginning().multiply(new BigDecimal(tournament.getParticipants().size()));
        for (TournamentAction tournamentAction : tournament.getTournamentActions()) {
            if (tournamentAction.getAction() == Action.PARTICIPANT_REBUY) {
                if (tournament.getChipsForReBuy() != null) {
                    totalStack = totalStack.add(tournament.getChipsForReBuy());
                } else {
                    return null;
                }
            } else if (tournamentAction.getAction() == Action.PARTICIPANT_ADDON) {
                if (tournament.getChipsForAddOn() != null) {
                    totalStack = totalStack.add(tournament.getChipsForAddOn());
                } else {
                    return null;
                }
            }
        }

        return totalStack.divide(new BigDecimal(getNonDroppedParticipants(tournament).size()), RoundingMode.HALF_UP).setScale(0, RoundingMode.HALF_UP);
    }

    public static BigDecimal getTotalPrizePool(Tournament tournament) {
        BigDecimal totalPrizePool = null;
        if (tournament.getPrizePool().getType() == PrizePoolType.CURRENCY) {
            totalPrizePool = BigDecimal.ZERO;
            for (Prize prize : tournament.getPrizePool().getPrizes()) {
                totalPrizePool = totalPrizePool.add(prize.getAmount().multiply(new BigDecimal(prize.getPlaceUntil() - prize.getPlaceFrom() + 1)));
            }

            return totalPrizePool;
        } else if (tournament.getPrizePool().getType() == PrizePoolType.PERCENTAGES) {
            if (tournament.getParticipants().size() == 0 || tournament.getBuyIn() == null) {
                return BigDecimal.ZERO;
            }

            totalPrizePool = tournament.getBuyIn().multiply(new BigDecimal(tournament.getParticipants().size()));
            for (TournamentAction tournamentAction : tournament.getTournamentActions()) {
                if (tournamentAction.getAction() == Action.PARTICIPANT_REBUY) {
                    if (tournament.getReBuy() != null) {
                        totalPrizePool = totalPrizePool.add(tournament.getReBuy());
                    } else {
                        return null;
                    }
                } else if (tournamentAction.getAction() == Action.PARTICIPANT_ADDON) {
                    if (tournament.getAddOn() != null) {
                        totalPrizePool = totalPrizePool.add(tournament.getAddOn());
                    } else {
                        return null;
                    }
                }
            }
        }

        return totalPrizePool;
    }

    public static int countActionsOfType(Tournament tournament, Action action) {
        int i = 0;
        for (TournamentAction tournamentAction : tournament.getTournamentActions()) {
            if (tournamentAction.getAction() == action) {
                i++;
            }
        }

        return i;
    }

    public static String getBigBlindOfLevel(Tournament tournament, int level) {
        for (Bet bet : tournament.getBlinds().getBets()) {
            if (bet.getLevel().equals(level)) {
                return bet.getBigBlind().stripTrailingZeros().toPlainString();
            }
        }

        return null;
    }

    public static String getSmallBlindOfLevel(Tournament tournament, int level) {
        for (Bet bet : tournament.getBlinds().getBets()) {
            if (bet.getLevel().equals(level)) {
                return bet.getSmallBlind().stripTrailingZeros().toPlainString();
            }
        }

        return null;
    }

    public static String getAnteOfLevel(Tournament tournament, int level) {
        for (Bet bet : tournament.getBlinds().getBets()) {
            if (bet.getLevel().equals(level)) {
                return bet.getAnte() != null ? bet.getAnte().stripTrailingZeros().toPlainString() : null;
            }
        }

        return null;
    }

}
