package lt.ktu.jonmic.android.activities.profile.participants;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import lt.ktu.jonmic.R;
import lt.ktu.jonmic.android.activities.BaseActivity;
import lt.ktu.jonmic.android.gui.GUIElementCreators;
import lt.ktu.jonmic.android.model.common.TaskResult;
import lt.ktu.jonmic.android.model.entities.Participant;
import lt.ktu.jonmic.android.tasks.HttpCallTask;

public class ViewParticipantActivity extends BaseActivity {

    public static final String EXTRA_PARTICIPANT_ID = "lt.ktu.jonmic.android.activities.profile.participants.ViewParticipantActivity.PARTICIPANT_ID";

    private TextView textViewFirstName;
    private TextView textViewLastName;
    private TextView textViewNickname;
    private Button buttonEdit;
    private Button buttonDelete;
    private Long participantId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.participant_view);

        textViewFirstName = (TextView) findViewById(R.id.participantViewTextViewFirstName);
        textViewLastName = (TextView) findViewById(R.id.participantViewTextViewLastName);
        textViewNickname = (TextView) findViewById(R.id.participantViewTextViewNickname);
        buttonEdit = (Button) findViewById(R.id.participantViewButtonEdit);
        buttonDelete = (Button) findViewById(R.id.participantViewButtonDelete);

        buttonEdit.setOnClickListener(new ButtonEditListener());
        buttonDelete.setOnClickListener(new ButtonDeleteListener());

        participantId = getIntent().getExtras().getLong(ViewParticipantActivity.EXTRA_PARTICIPANT_ID);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new GetParticipantTask().execute();
    }

    class ButtonEditListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(ViewParticipantActivity.this, EditParticipantActivity.class);
            intent.putExtra(EditParticipantActivity.EXTRA_PARTICIPANT_ID, participantId);
            startActivity(intent);
        }

    }

    class ButtonDeleteListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            GUIElementCreators.showAlertDialog(ViewParticipantActivity.this, getString(R.string.common_deleteConfirmation), new ButtonConfirmDeleteListener());
        }

        class ButtonConfirmDeleteListener implements DialogInterface.OnClickListener {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                new DeleteParticipantTask().execute();
            }

        }

    }

    class GetParticipantTask extends HttpCallTask<Participant> {

        public GetParticipantTask() {
            super(ViewParticipantActivity.this, "/participant/" + participantId, GET, FORM_URLENCODED, JSON, null, Participant.class, false, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<Participant> result) {
            Participant participant = result.getResult();

            textViewFirstName.setText(participant.getFirstName());
            textViewLastName.setText(participant.getLastName());
            textViewNickname.setText(participant.getNickname());
        }

    }

    class DeleteParticipantTask extends HttpCallTask<Boolean> {

        public DeleteParticipantTask() {
            super(ViewParticipantActivity.this, "/participant/" + participantId, DELETE, FORM_URLENCODED, JSON, null, Boolean.class, false, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<Boolean> result) {
            if (result.getError() == null) {
                Toast.makeText(ViewParticipantActivity.this, R.string.participant_participantSuccessfullyDeleted, Toast.LENGTH_LONG).show();
                finish();
            }
        }

    }

}
