package lt.ktu.jonmic.android.activities;

import android.app.Activity;

public abstract class BaseActivity extends Activity {

    protected static final String GET = "GET";
    protected static final String POST = "POST";
    protected static final String PUT = "PUT";
    protected static final String DELETE = "DELETE";

    protected static final String JSON = "application/json";
    protected static final String FORM_URLENCODED = "application/x-www-form-urlencoded";

}
