package lt.ktu.jonmic.android.model.common;

import java.util.List;

public class ServerError extends RuntimeException {

    private String header;
    private List<String> items;

    public ServerError() {
    }

    public ServerError(String header, List<String> items) {
        this.header = header;
        this.items = items;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public List<String> getItems() {
        return items;
    }

    public void setItems(List<String> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        StringBuffer result = new StringBuffer();
        result.append(header);
        result.append(!items.isEmpty() ? ":" : "");
        for (String item : items) {
            result.append("\n").append("• ").append(item);
        }

        return result.toString();
    }

}
