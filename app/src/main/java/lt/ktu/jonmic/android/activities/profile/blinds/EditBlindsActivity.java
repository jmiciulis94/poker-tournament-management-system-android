package lt.ktu.jonmic.android.activities.profile.blinds;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import java.math.BigDecimal;
import lt.ktu.jonmic.R;
import lt.ktu.jonmic.android.activities.BaseActivity;
import lt.ktu.jonmic.android.activities.tournaments.EditTournamentActivity;
import lt.ktu.jonmic.android.model.common.TaskResult;
import lt.ktu.jonmic.android.model.entities.Bet;
import lt.ktu.jonmic.android.model.entities.Blinds;
import lt.ktu.jonmic.android.tasks.HttpCallTask;
import lt.ktu.jonmic.android.utilities.MarshallingUtils;

public class EditBlindsActivity extends BaseActivity {

    public static final String EXTRA_BLINDS_ID = "lt.ktu.jonmic.android.activities.profile.blinds.EditBlindsActivity.BLINDS_ID";

    private EditText editTextCaption;
    private EditText editTextNumberOfLevels;
    private TableLayout tableLayoutBlinds;
    private Button buttonSave;
    private Long blindsId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.blinds_edit);

        editTextCaption = (EditText) findViewById(R.id.blindsEditEditTextCaption);
        editTextNumberOfLevels = (EditText) findViewById(R.id.blindsEditEditTextNumberOfLevels);
        tableLayoutBlinds = (TableLayout) findViewById(R.id.blindsEditTableLayoutBlinds);
        buttonSave = (Button) findViewById(R.id.blindsEdittButtonSave);

        editTextNumberOfLevels.addTextChangedListener(new EditTextNumberOfLevelsListener());
        buttonSave.setOnClickListener(new ButtonSaveListener());

        if (getIntent().hasExtra(EditBlindsActivity.EXTRA_BLINDS_ID)) {
            blindsId = getIntent().getExtras().getLong(EditBlindsActivity.EXTRA_BLINDS_ID);
            new GetBlindsTask().execute();
        }
    }

    class EditTextNumberOfLevelsListener implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            String numberOfLevelsText = editTextNumberOfLevels.getText().toString();
            int numberOfLevels = !numberOfLevelsText.isEmpty() ? Integer.valueOf(numberOfLevelsText) : 0;

            if (numberOfLevels > tableLayoutBlinds.getChildCount() - 1) {
                for (int i = tableLayoutBlinds.getChildCount() - 1; i < numberOfLevels; i++) {
                    LayoutInflater inflater = (LayoutInflater) EditBlindsActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    TableRow row = (TableRow) inflater.inflate(R.layout.blinds_edit_row, null);
                    ((TextView) row.findViewById(R.id.blindsEditRowTextViewLevel)).setText(String.valueOf(i + 1));
                    tableLayoutBlinds.addView(row);
                }
            } else {
                for (int i = tableLayoutBlinds.getChildCount() - 1; i > numberOfLevels; i--) {
                    tableLayoutBlinds.removeViewAt(i);
                }
            }
        }

    }

    class ButtonSaveListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            Blinds blinds = new Blinds();

            String caption = editTextCaption.getText().toString();

            blinds.setId(blindsId);
            blinds.setCaption(caption);

            for (int i = 1; i < tableLayoutBlinds.getChildCount(); i++) {
                TableRow tableRow = (TableRow) tableLayoutBlinds.getChildAt(i);
                String level = ((TextView) tableRow.findViewById(R.id.blindsEditRowTextViewLevel)).getText().toString();
                String bigBlind = ((EditText) tableRow.findViewById(R.id.blindsEditRowEditTextBigBlind)).getText().toString();
                String smallBlind = ((EditText) tableRow.findViewById(R.id.blindsEditRowEditTextSmallBlind)).getText().toString();
                String ante = ((EditText) tableRow.findViewById(R.id.blindsEditRowEditTextAnte)).getText().toString();

                Bet bet = new Bet();
                bet.setLevel(!level.isEmpty() ? Integer.valueOf(level) : null);
                bet.setBigBlind(!bigBlind.isEmpty() ? new BigDecimal(bigBlind) : null);
                bet.setSmallBlind(!smallBlind.isEmpty() ? new BigDecimal(smallBlind) : null);
                bet.setAnte(!ante.isEmpty() ? new BigDecimal(ante) : null);

                blinds.getBets().add(bet);
            }

            String json = MarshallingUtils.convertObjectToJsonBytes(blinds);
            new SaveBlindsTask(json).execute();
        }

    }

    class GetBlindsTask extends HttpCallTask<Blinds> {

        public GetBlindsTask() {
            super(EditBlindsActivity.this, "/blinds/" + blindsId, GET, FORM_URLENCODED, JSON, null, Blinds.class, false, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<Blinds> result) {
            Blinds blinds = result.getResult();

            editTextCaption.setText(blinds.getCaption());
            for (int i = 0; i < blinds.getBets().size(); i++) {
                Bet bet = blinds.getBets().get(i);

                LayoutInflater inflater = (LayoutInflater) EditBlindsActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                TableRow row = (TableRow) inflater.inflate(R.layout.blinds_edit_row, null);

                ((TextView) row.findViewById(R.id.blindsEditRowTextViewLevel)).setText(String.valueOf(bet.getLevel()));
                ((EditText) row.findViewById(R.id.blindsEditRowEditTextBigBlind)).setText(bet.getBigBlind().stripTrailingZeros().toPlainString());
                ((EditText) row.findViewById(R.id.blindsEditRowEditTextSmallBlind)).setText(bet.getSmallBlind().stripTrailingZeros().toPlainString());
                ((EditText) row.findViewById(R.id.blindsEditRowEditTextAnte)).setText(bet.getAnte() != null ? bet.getAnte().stripTrailingZeros().toPlainString() : "");

                tableLayoutBlinds.addView(row);
            }

            editTextNumberOfLevels.setText(String.valueOf(blinds.getBets().size()));
        }

    }

    class SaveBlindsTask extends HttpCallTask<Blinds> {


        public SaveBlindsTask(String data) {
            super(EditBlindsActivity.this, "/blinds", PUT, JSON, JSON, data, Blinds.class, false, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<Blinds> result) {
            if (result.getError() == null) {
                Toast.makeText(EditBlindsActivity.this, R.string.blinds_blindsSuccessfullySaved, Toast.LENGTH_LONG).show();

                Intent intent = new Intent();
                intent.putExtra(EditTournamentActivity.EXTRA_BLINDS, result.getResult());
                setResult(Activity.RESULT_OK, intent);

                finish();
            }
        }

    }

}
