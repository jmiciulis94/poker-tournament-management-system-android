package lt.ktu.jonmic.android.activities.profile;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import lt.ktu.jonmic.R;
import lt.ktu.jonmic.android.activities.BaseActivity;
import lt.ktu.jonmic.android.activities.profile.blinds.BlindsListActivity;
import lt.ktu.jonmic.android.activities.profile.chips.PokerChipsSetsListActivity;
import lt.ktu.jonmic.android.activities.profile.participants.ParticipantsListActivity;
import lt.ktu.jonmic.android.activities.profile.pauses.PausesListActivity;
import lt.ktu.jonmic.android.activities.profile.prizes.PrizePoolsListActivity;
import lt.ktu.jonmic.android.activities.users.EditUserActivity;

public class ProfileActivity extends BaseActivity {

    private ListView listViewMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);

        listViewMenu = (ListView) findViewById(R.id.profileListViewMenu);

        listViewMenu.setOnItemClickListener(new ListViewMenuListener());
        listViewMenu.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, getMenuItems()));
    }

    private List<String> getMenuItems() {
        List<String> menuList = new ArrayList<>();
        menuList.add(getString(R.string.profile_menu_personalData).toUpperCase(Locale.getDefault()));
        menuList.add(getString(R.string.profile_menu_blinds).toUpperCase(Locale.getDefault()));
        menuList.add(getString(R.string.profile_menu_pauses).toUpperCase(Locale.getDefault()));
        menuList.add(getString(R.string.profile_menu_pokerChipsSets).toUpperCase(Locale.getDefault()));
        menuList.add(getString(R.string.profile_menu_prizePools).toUpperCase(Locale.getDefault()));
        menuList.add(getString(R.string.profile_menu_participants).toUpperCase(Locale.getDefault()));

        return menuList;
    }

    class ListViewMenuListener implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            String title = (String) parent.getItemAtPosition(position);

            if (title.equalsIgnoreCase(getString(R.string.profile_menu_personalData))) {
                Intent intent = new Intent(ProfileActivity.this, EditUserActivity.class);
                intent.putExtra(EditUserActivity.EXTRA_EDIT_USER, true);
                startActivity(intent);
            } else if (title.equalsIgnoreCase(getString(R.string.profile_menu_blinds))) {
                startActivity(new Intent(ProfileActivity.this, BlindsListActivity.class));
            } else if (title.equalsIgnoreCase(getString(R.string.profile_menu_pauses))) {
                startActivity(new Intent(ProfileActivity.this, PausesListActivity.class));
            } else if (title.equalsIgnoreCase(getString(R.string.profile_menu_pokerChipsSets))) {
                startActivity(new Intent(ProfileActivity.this, PokerChipsSetsListActivity.class));
            } else if (title.equalsIgnoreCase(getString(R.string.profile_menu_prizePools))) {
                startActivity(new Intent(ProfileActivity.this, PrizePoolsListActivity.class));
            } else if (title.equalsIgnoreCase(getString(R.string.profile_menu_participants))) {
                startActivity(new Intent(ProfileActivity.this, ParticipantsListActivity.class));
            }
        }

    }

}
