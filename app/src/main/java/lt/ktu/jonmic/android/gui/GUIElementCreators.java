package lt.ktu.jonmic.android.gui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.WindowManager;
import lt.ktu.jonmic.R;

public class GUIElementCreators {

    public static void showAlertDialog(Context context, String title, DialogInterface.OnClickListener positiveButtonListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setCancelable(false);
        builder.setPositiveButton(context.getString(R.string.common_ok), positiveButtonListener);
        builder.setNegativeButton(context.getString(R.string.common_cancel), null);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public static void showAlertDialog(Context context, View view, DialogInterface.OnClickListener positiveButtonListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(view);
        builder.setCancelable(true);
        builder.setPositiveButton(context.getString(R.string.common_ok), positiveButtonListener);
        builder.setNegativeButton(context.getString(R.string.common_cancel), null);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    public static void showDialogWithDropDownItems(Context context, String title, String[] items, DialogInterface.OnClickListener itemSelectListener, String positiveButtonLabel, DialogInterface.OnClickListener positiveButtonListener, String negativeButtonLabel) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setCancelable(true);
        builder.setSingleChoiceItems(items, 0, itemSelectListener);
        builder.setPositiveButton(positiveButtonLabel, positiveButtonListener);
        builder.setNegativeButton(negativeButtonLabel, null);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

}
