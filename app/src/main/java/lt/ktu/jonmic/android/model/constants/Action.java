package lt.ktu.jonmic.android.model.constants;

import android.content.Context;
import java.util.Locale;

public enum Action {

    START, FINISH, LEVEL_UP, PAUSE, RESUME, RESTART_FROM_LEVEL, PARTICIPANT_DROP, PARTICIPANT_REBUY, PARTICIPANT_ADDON;

    public static String getActionName(Context context, Action action, String details) {
        if (action == Action.RESTART_FROM_LEVEL || action == Action.PARTICIPANT_DROP || action == Action.PARTICIPANT_REBUY || action == Action.PARTICIPANT_ADDON) {
            return context.getString(context.getResources().getIdentifier("action." + action.name().toLowerCase(Locale.getDefault()), "string", context.getPackageName()), details);
        } else {
            return context.getString(context.getResources().getIdentifier("action." + action.name().toLowerCase(Locale.getDefault()), "string", context.getPackageName()));
        }
    }

}
