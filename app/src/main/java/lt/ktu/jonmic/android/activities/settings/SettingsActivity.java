package lt.ktu.jonmic.android.activities.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import java.text.SimpleDateFormat;
import java.util.Locale;
import lt.ktu.jonmic.android.activities.BaseActivity;
import lt.ktu.jonmic.android.fragments.SettingsFragment;

public class SettingsActivity extends BaseActivity {

    private static final String SERVER_URL = "http://10.0.2.2:8080/api";
    private static final String DATE_FORMAT_US = "MM/dd/yyyy";
    private static final String DATE_FORMAT_RU = "dd/MM/yyyy";
    private static final String DATE_FORMAT_EU = "yyyy-MM-dd";
    private static final String DATETIME_FORMAT_US = "MM/dd/yyyy HH:mm";
    private static final String DATETIME_FORMAT_RU = "dd/MM/yyyy HH:mm";
    private static final String DATETIME_FORMAT_EU = "yyyy-MM-dd HH:mm";

    public static String getServerURL() {
        return SERVER_URL;
    }

    public static String getUsername(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(SettingsFragment.KEY_LOGIN_USERNAME, "");
    }

    public static void setUsername(Context context, String username) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(SettingsFragment.KEY_LOGIN_USERNAME, username);
        editor.commit();
    }

    public static String getPassword(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(SettingsFragment.KEY_LOGIN_PASSWORD, "");
    }

    public static void setPassword(Context context, String password) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(SettingsFragment.KEY_LOGIN_PASSWORD, password);
        editor.commit();
    }

    public static boolean isRememberMe(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(SettingsFragment.KEY_LOGIN_REMEMBER_ME, false);
    }

    public static void setRememberMe(Context context, boolean rememberMe) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(SettingsFragment.KEY_LOGIN_REMEMBER_ME, rememberMe);
        editor.commit();
    }

    public static String getCurrency(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(SettingsFragment.KEY_INTERNATIONALIZATION_CURRENCY, "");
    }

    public static String getMaxNumberOfParticipantsToShow(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(SettingsFragment.KEY_DASHBOARD_MAX_NUMBER_OF_PARTICIPANTS, "0");
    }

    public static String getMaxNumberOfPrizesToShow(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(SettingsFragment.KEY_DASHBOARD_MAX_NUMBER_OF_PRIZES, "0");
    }

    public static boolean showTimeForReBuys(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(SettingsFragment.KEY_DASHBOARD_SHOW_TIME_FOR_REBUYS, true);
    }

    public static boolean showTimeUntilPause(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(SettingsFragment.KEY_DASHBOARD_SHOW_TIME_UNTIL_PAUSE, true);
    }

    public static boolean showTournamentCaption(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(SettingsFragment.KEY_DASHBOARD_SHOW_TOURNAMENT_CAPTION, true);
    }

    public static boolean showTournamentStartTime(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(SettingsFragment.KEY_DASHBOARD_SHOW_TOURNAMENT_START_TIME, true);
    }

    public static boolean showPrices(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(SettingsFragment.KEY_DASHBOARD_SHOW_PRICES, true);
    }

    public static boolean showLevelTime(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(SettingsFragment.KEY_DASHBOARD_SHOW_LEVEL_TIME, true);
    }

    public static boolean showNumberOfParticipants(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(SettingsFragment.KEY_DASHBOARD_SHOW_NUMBER_OF_PARTICIPANTS, true);
    }

    public static boolean showAverageStack(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(SettingsFragment.KEY_DASHBOARD_SHOW_AVERAGE_STACK, true);
    }

    public static boolean showTotalReBuysAndAddOns(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(SettingsFragment.KEY_DASHBOARD_SHOW_TOTAL_REBUYS_AND_ADDONS, true);
    }

    public static boolean showChipValues(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(SettingsFragment.KEY_DASHBOARD_SHOW_CHIP_VALUES, true);
    }

    public static boolean showParticipants(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(SettingsFragment.KEY_DASHBOARD_SHOW_PARTICIPANTS, true);
    }

    public static boolean showPrizePool(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(SettingsFragment.KEY_DASHBOARD_SHOW_PRIZE_POOL, true);
    }

    public static boolean showButtons(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(SettingsFragment.KEY_DASHBOARD_SHOW_BUTTONS, true);
    }

    public static SimpleDateFormat getDateFormat(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String value = sharedPreferences.getString(SettingsFragment.KEY_INTERNATIONALIZATION_DATE_FORMAT, null);

        if (value.equals(SettingsFragment.VALUE_DATE_FORMAT_US)) {
            return new SimpleDateFormat(DATE_FORMAT_US, Locale.getDefault());
        } else if (value.equals(SettingsFragment.VALUE_DATE_FORMAT_RU)) {
            return new SimpleDateFormat(DATE_FORMAT_RU, Locale.getDefault());
        } else if (value.equals(SettingsFragment.VALUE_DATE_FORMAT_EU)) {
            return new SimpleDateFormat(DATE_FORMAT_EU, Locale.getDefault());
        }

        return null;
    }

    public static SimpleDateFormat getDateTimeFormat(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String value = sharedPreferences.getString(SettingsFragment.KEY_INTERNATIONALIZATION_DATE_FORMAT, null);

        if (value.equals(SettingsFragment.VALUE_DATE_FORMAT_US)) {
            return new SimpleDateFormat(DATETIME_FORMAT_US, Locale.getDefault());
        } else if (value.equals(SettingsFragment.VALUE_DATE_FORMAT_RU)) {
            return new SimpleDateFormat(DATETIME_FORMAT_RU, Locale.getDefault());
        } else if (value.equals(SettingsFragment.VALUE_DATE_FORMAT_EU)) {
            return new SimpleDateFormat(DATETIME_FORMAT_EU, Locale.getDefault());
        }

        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new SettingsFragment()).commit();
    }

}
