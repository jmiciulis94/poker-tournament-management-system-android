package lt.ktu.jonmic.android.model.entities;

import java.math.BigDecimal;
import lt.ktu.jonmic.android.model.constants.Color;

public class Chip extends AbstractEntity {

    private Long id;
    private Color color;
    private BigDecimal value;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

}
