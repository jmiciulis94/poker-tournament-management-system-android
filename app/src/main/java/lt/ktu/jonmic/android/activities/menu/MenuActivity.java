package lt.ktu.jonmic.android.activities.menu;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import lt.ktu.jonmic.R;
import lt.ktu.jonmic.android.activities.BaseActivity;
import lt.ktu.jonmic.android.activities.profile.ProfileActivity;
import lt.ktu.jonmic.android.activities.settings.SettingsActivity;
import lt.ktu.jonmic.android.activities.tournaments.TournamentActivity;
import lt.ktu.jonmic.android.activities.tournaments.TournamentsListActivity;
import lt.ktu.jonmic.android.activities.users.LoginActivity;
import lt.ktu.jonmic.android.gui.GUIElementCreators;
import lt.ktu.jonmic.android.model.common.TaskResult;
import lt.ktu.jonmic.android.model.entities.Tournament;
import lt.ktu.jonmic.android.tasks.HttpCallTask;
import lt.ktu.jonmic.android.utilities.TournamentUtils;

public class MenuActivity extends BaseActivity {

    private ListView listViewMenu;
    private String sessionId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu);

        listViewMenu = (ListView) findViewById(R.id.menuListViewMenu);

        listViewMenu.setOnItemClickListener(new ListViewMenuListener());
        listViewMenu.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, getMenuItems()));

        SharedPreferences sharedPreferences = getSharedPreferences(MenuActivity.this.getPackageName(), Context.MODE_PRIVATE);
        sessionId = sharedPreferences.getString(LoginActivity.EXTRA_JSESSION_ID, null);
    }

    @Override
    public void onBackPressed() {
        if (sessionId != null) {
            new LogoutTask().execute();
        }

        super.onBackPressed();
    }

    private List<String> getMenuItems() {
        List<String> menuList = new ArrayList<>();
        menuList.add(getString(R.string.menu_openTournament).toUpperCase(Locale.getDefault()));
        menuList.add(getString(R.string.menu_myTournaments).toUpperCase(Locale.getDefault()));
        menuList.add(getString(R.string.menu_myProfile).toUpperCase(Locale.getDefault()));
        menuList.add(getString(R.string.menu_settings).toUpperCase(Locale.getDefault()));
        menuList.add(getString(R.string.menu_logout).toUpperCase(Locale.getDefault()));

        return menuList;
    }

    class ListViewMenuListener implements AdapterView.OnItemClickListener {

        private EditText editTextTournamentId;
        private EditText editTextTournamentPassword;

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            String title = (String) parent.getItemAtPosition(position);

            if (sessionId == null) {
                if (title.equalsIgnoreCase(getString(R.string.menu_myTournaments)) || title.equalsIgnoreCase(getString(R.string.menu_myProfile))) {
                    Toast.makeText(MenuActivity.this, R.string.menu_thisFunctionIsDisabled, Toast.LENGTH_LONG).show();
                    return;
                }
            }

            if (title.equalsIgnoreCase(getString(R.string.menu_openTournament))) {
                LayoutInflater inflater = MenuActivity.this.getLayoutInflater();
                View joinTournamentPopup = inflater.inflate(R.layout.join_tournament_popup, null);
                editTextTournamentId = (EditText) joinTournamentPopup.findViewById(R.id.joinTournamentPopUpEditTextTournamentId);
                editTextTournamentPassword = (EditText) joinTournamentPopup.findViewById(R.id.joinTournamentPopUpEditTextTournamentPassword);
                GUIElementCreators.showAlertDialog(MenuActivity.this, joinTournamentPopup, new ButtonOkListener());
            } else if (title.equalsIgnoreCase(getString(R.string.menu_myTournaments))) {
                startActivity(new Intent(MenuActivity.this, TournamentsListActivity.class));
            } else if (title.equalsIgnoreCase(getString(R.string.menu_myProfile))) {
                startActivity(new Intent(MenuActivity.this, ProfileActivity.class));
            } else if (title.equalsIgnoreCase(getString(R.string.menu_settings))) {
                startActivity(new Intent(MenuActivity.this, SettingsActivity.class));
            } else if (title.equalsIgnoreCase(getString(R.string.menu_logout)) && sessionId != null) {
                new LogoutTask().execute();
            } else if (title.equalsIgnoreCase(getString(R.string.menu_logout)) && sessionId == null) {
                finish();
            }
        }

        class ButtonOkListener implements DialogInterface.OnClickListener {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Long tournamentId = Long.valueOf(editTextTournamentId.getText().toString());
                String tournamentPassword = editTextTournamentPassword.getText().toString();
                new CheckTournamentTask(tournamentId, tournamentPassword).execute();
            }

        }

    }

    class CheckTournamentTask extends HttpCallTask<Tournament> {

        public CheckTournamentTask(long tournamentId, String tournamentPassword) {
            super(MenuActivity.this, "/tournament/" + tournamentId + "?password=" + (tournamentPassword != null ? tournamentPassword : ""), GET, FORM_URLENCODED, JSON, null, Tournament.class, false, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<Tournament> result) {
            Tournament tournament = result.getResult();
            if (tournament != null && TournamentUtils.getTournamentFinishDate(tournament) != null) {
                Toast.makeText(MenuActivity.this, R.string.menu_tournamentAlreadyFinished, Toast.LENGTH_LONG).show();
            } else if (tournament != null) {
                Intent intent = new Intent(MenuActivity.this, TournamentActivity.class);
                intent.putExtra(TournamentActivity.EXTRA_TOURNAMENT_ID, tournament.getId());
                intent.putExtra(TournamentActivity.EXTRA_TOURNAMENT_PASSWORD, tournament.getPassword());
                startActivity(intent);
            }
        }

    }

    class LogoutTask extends HttpCallTask<Boolean> {

        public LogoutTask() {
            super(MenuActivity.this, "/logout", POST, FORM_URLENCODED, JSON, null, Boolean.class, false, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<Boolean> result) {
            if (result.getError() == null) {
                Toast.makeText(MenuActivity.this, R.string.menu_loggedOutSuccessfully, Toast.LENGTH_LONG).show();
                finish();
            }
        }

    }

}
