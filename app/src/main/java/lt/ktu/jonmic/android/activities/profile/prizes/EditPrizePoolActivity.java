package lt.ktu.jonmic.android.activities.profile.prizes;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;
import java.math.BigDecimal;
import java.util.Arrays;
import lt.ktu.jonmic.R;
import lt.ktu.jonmic.android.activities.BaseActivity;
import lt.ktu.jonmic.android.activities.tournaments.EditTournamentActivity;
import lt.ktu.jonmic.android.model.common.TaskResult;
import lt.ktu.jonmic.android.model.constants.PrizePoolType;
import lt.ktu.jonmic.android.model.entities.Prize;
import lt.ktu.jonmic.android.model.entities.PrizePool;
import lt.ktu.jonmic.android.tasks.HttpCallTask;
import lt.ktu.jonmic.android.utilities.MarshallingUtils;

public class EditPrizePoolActivity extends BaseActivity {

    public static final String EXTRA_PRIZE_POOL_ID = "lt.ktu.jonmic.android.activities.profile.prizes.EditPrizePoolActivity.PRIZE_POOL_ID";

    private EditText editTextCaption;
    private EditText editTextRake;
    private EditText editTextNumberOfPrizes;
    private Spinner spinnerType;
    private TableLayout tableLayoutPrizes;
    private LinearLayout linearLayoutRake;
    private Button buttonSave;
    private Long prizePoolId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.prize_pool_edit);

        editTextCaption = (EditText) findViewById(R.id.prizePoolEditEditTextCaption);
        editTextRake = (EditText) findViewById(R.id.prizePoolEditEditTextRake);
        editTextNumberOfPrizes = (EditText) findViewById(R.id.prizePoolEditEditTextNumberOfPrizes);
        spinnerType = (Spinner) findViewById(R.id.prizePoolEditSpinnerType);
        tableLayoutPrizes = (TableLayout) findViewById(R.id.prizePoolEditTableLayoutPrizes);
        linearLayoutRake = (LinearLayout) findViewById(R.id.prizePoolEditLinearLayoutRake);
        buttonSave = (Button) findViewById(R.id.prizePoolEdittButtonSave);

        ArrayAdapter<String> spinnerTypeDataAdapter = new ArrayAdapter<>(EditPrizePoolActivity.this, android.R.layout.simple_spinner_dropdown_item, PrizePoolType.getPrizePoolTypeNames(EditPrizePoolActivity.this));
        spinnerType.setAdapter(spinnerTypeDataAdapter);
        linearLayoutRake.setVisibility(View.GONE);

        editTextNumberOfPrizes.addTextChangedListener(new EditTextNumberOfPrizesListener());
        spinnerType.setOnItemSelectedListener(new SpinnerTypeListener());
        buttonSave.setOnClickListener(new ButtonSaveListener());

        if (getIntent().hasExtra(EditPrizePoolActivity.EXTRA_PRIZE_POOL_ID)) {
            prizePoolId = getIntent().getExtras().getLong(EditPrizePoolActivity.EXTRA_PRIZE_POOL_ID);
            new GetPrizePoolTask().execute();
        }
    }

    class EditTextNumberOfPrizesListener implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            String numberOfPrizesText = editTextNumberOfPrizes.getText().toString();
            int numberOfPrizes = !numberOfPrizesText.isEmpty() ? Integer.valueOf(numberOfPrizesText) : 0;

            if (numberOfPrizes > tableLayoutPrizes.getChildCount() - 1) {
                for (int i = tableLayoutPrizes.getChildCount() - 1; i < numberOfPrizes; i++) {
                    LayoutInflater inflater = (LayoutInflater) EditPrizePoolActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    TableRow row = (TableRow) inflater.inflate(R.layout.prize_pool_edit_row, null);
                    tableLayoutPrizes.addView(row);
                }
            } else {
                for (int i = tableLayoutPrizes.getChildCount() - 1; i > numberOfPrizes; i--) {
                    tableLayoutPrizes.removeViewAt(i);
                }
            }
        }

    }

    class SpinnerTypeListener implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            String prizePoolTypeLabel = (String) parent.getItemAtPosition(position);
            PrizePoolType prizePoolType = PrizePoolType.getPrizePoolTypeByName(EditPrizePoolActivity.this, prizePoolTypeLabel);
            if (prizePoolType == PrizePoolType.CURRENCY) {
                linearLayoutRake.setVisibility(View.GONE);
                editTextRake.setText("");
            } else if (prizePoolType == PrizePoolType.PERCENTAGES) {
                linearLayoutRake.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> view) {
        }

    }

    class ButtonSaveListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            PrizePool prizePool = new PrizePool();

            String caption = editTextCaption.getText().toString();
            String rake = editTextRake.getText().toString();

            prizePool.setId(prizePoolId);
            prizePool.setCaption(caption);
            prizePool.setType(PrizePoolType.getPrizePoolTypeByName(EditPrizePoolActivity.this, (String) spinnerType.getSelectedItem()));
            prizePool.setRake(!rake.isEmpty() ? new BigDecimal(rake) : null);

            for (int i = 1; i < tableLayoutPrizes.getChildCount(); i++) {
                TableRow tableRow = (TableRow) tableLayoutPrizes.getChildAt(i);
                String placeFrom = ((EditText) tableRow.findViewById(R.id.prizePoolEditRowEditTextPlaceFrom)).getText().toString();
                String placeUntil = ((EditText) tableRow.findViewById(R.id.prizePoolEditRowEditTextPlaceUntil)).getText().toString();
                String amount = ((EditText) tableRow.findViewById(R.id.prizePoolEditRowEditTextAmount)).getText().toString();

                Prize prize = new Prize();
                prize.setPlaceFrom(!placeFrom.isEmpty() ? Integer.valueOf(placeFrom) : null);
                prize.setPlaceUntil(!placeUntil.isEmpty() ? Integer.valueOf(placeUntil) : null);
                prize.setAmount(!amount.isEmpty() ? new BigDecimal(amount) : null);

                prizePool.getPrizes().add(prize);
            }

            String json = MarshallingUtils.convertObjectToJsonBytes(prizePool);
            new SavePrizePoolTask(json).execute();
        }

    }

    class GetPrizePoolTask extends HttpCallTask<PrizePool> {

        public GetPrizePoolTask() {
            super(EditPrizePoolActivity.this, "/prize-pool/" + prizePoolId, GET, FORM_URLENCODED, JSON, null, PrizePool.class, false, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<PrizePool> result) {
            PrizePool prizePool = result.getResult();

            editTextCaption.setText(prizePool.getCaption());
            editTextRake.setText(prizePool.getRake() != null ? prizePool.getRake().stripTrailingZeros().toPlainString() : "");
            spinnerType.setSelection(Arrays.asList(PrizePoolType.values()).indexOf(prizePool.getType()));

            for (int i = 0; i < prizePool.getPrizes().size(); i++) {
                Prize prize = prizePool.getPrizes().get(i);

                LayoutInflater inflater = (LayoutInflater) EditPrizePoolActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                TableRow row = (TableRow) inflater.inflate(R.layout.prize_pool_edit_row, null);

                ((EditText) row.findViewById(R.id.prizePoolEditRowEditTextPlaceFrom)).setText(String.valueOf(prize.getPlaceFrom()));
                ((EditText) row.findViewById(R.id.prizePoolEditRowEditTextPlaceUntil)).setText(String.valueOf(prize.getPlaceUntil()));
                ((EditText) row.findViewById(R.id.prizePoolEditRowEditTextAmount)).setText(prize.getAmount().stripTrailingZeros().toPlainString());

                tableLayoutPrizes.addView(row);
            }

            editTextNumberOfPrizes.setText(String.valueOf(prizePool.getPrizes().size()));
        }

    }

    class SavePrizePoolTask extends HttpCallTask<PrizePool> {


        public SavePrizePoolTask(String data) {
            super(EditPrizePoolActivity.this, "/prize-pool", PUT, JSON, JSON, data, PrizePool.class, false, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<PrizePool> result) {
            if (result.getError() == null) {
                Toast.makeText(EditPrizePoolActivity.this, R.string.prizePool_prizePoolSuccessfullySaved, Toast.LENGTH_LONG).show();

                Intent intent = new Intent();
                intent.putExtra(EditTournamentActivity.EXTRA_PRIZE_POOL, result.getResult());
                setResult(Activity.RESULT_OK, intent);

                finish();
            }
        }

    }

}
