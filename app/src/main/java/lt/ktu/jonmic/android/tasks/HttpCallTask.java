package lt.ktu.jonmic.android.tasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.widget.Toast;
import java.io.IOException;
import java.util.ArrayList;
import lt.ktu.jonmic.R;
import lt.ktu.jonmic.android.activities.settings.SettingsActivity;
import lt.ktu.jonmic.android.activities.users.LoginActivity;
import lt.ktu.jonmic.android.model.common.ServerError;
import lt.ktu.jonmic.android.model.common.TaskResult;
import lt.ktu.jonmic.android.utilities.HttpUtils;

public abstract class HttpCallTask<T> extends AsyncTask<String, Void, TaskResult<T>> {

    protected Activity activity;
    protected ProgressDialog progressDialog;
    protected String httpAddress;
    protected String httpMethod;
    protected String acceptType;
    protected String contentType;
    protected String data;
    protected Class resultClass;
    protected boolean isList;
    protected boolean showSpinner;

    public HttpCallTask(Activity activity, String httpAddress, String httpMethod, String contentType, String acceptType, String data, Class resultClass, boolean isList, boolean showSpinner) {
        this.activity = activity;
        this.httpAddress = httpAddress;
        this.httpMethod = httpMethod;
        this.acceptType = acceptType;
        this.contentType = contentType;
        this.data = data;
        this.resultClass = resultClass;
        this.isList = isList;
        this.showSpinner = showSpinner;
    }

    @Override
    protected void onPreExecute() {
        if (showSpinner) {
            progressDialog = ProgressDialog.show(activity, "", activity.getString(R.string.common_pleaseWait), true);
        }
    }

    @Override
    protected TaskResult<T> doInBackground(String... params) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(activity.getPackageName(), Context.MODE_PRIVATE);
        String sessionId = sharedPreferences.getString(LoginActivity.EXTRA_JSESSION_ID, null);

        try {
            return HttpUtils.doHttpCall(SettingsActivity.getServerURL() + httpAddress, httpMethod, contentType, acceptType, data, resultClass, isList, sessionId);
        } catch (SecurityException ex) {
            return new TaskResult<T>(new ServerError(activity.getString(R.string.common_sessionFinished), new ArrayList<String>()));
        } catch (IOException ex) {
            return new TaskResult<T>(new ServerError(activity.getString(R.string.common_canNotConnectToTheServer), new ArrayList<String>()));
        }
    }

    @Override
    protected void onPostExecute(TaskResult<T> result) {
        if (showSpinner && !activity.isDestroyed()) {
            progressDialog.dismiss();
        }

        if (result.getError() == null) {
            postExecuteActions(result);
        } else {
            Toast.makeText(activity, result.getError().toString(), Toast.LENGTH_LONG).show();
        }
    }

    protected abstract void postExecuteActions(TaskResult<T> result);

}

