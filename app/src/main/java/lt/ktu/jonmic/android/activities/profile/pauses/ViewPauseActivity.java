package lt.ktu.jonmic.android.activities.profile.pauses;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import lt.ktu.jonmic.R;
import lt.ktu.jonmic.android.activities.BaseActivity;
import lt.ktu.jonmic.android.gui.GUIElementCreators;
import lt.ktu.jonmic.android.model.common.TaskResult;
import lt.ktu.jonmic.android.model.entities.Pause;
import lt.ktu.jonmic.android.tasks.HttpCallTask;

public class ViewPauseActivity extends BaseActivity {

    public static final String EXTRA_PAUSE_ID = "lt.ktu.jonmic.android.activities.profile.pauses.ViewPauseActivity.PAUSE_ID";

    private TextView textViewCaption;
    private TextView textViewPauseMinute;
    private TextView textViewNumberOfLevels;
    private TextView textViewPauseLength;
    private TextView textViewAddOnPauseLength;
    private Button buttonEdit;
    private Button buttonDelete;
    private Long pauseId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pause_view);

        textViewCaption = (TextView) findViewById(R.id.pauseViewTextViewCaption);
        textViewPauseMinute = (TextView) findViewById(R.id.pauseViewTextViewPauseMinute);
        textViewNumberOfLevels = (TextView) findViewById(R.id.pauseViewTextViewNumberOfLevels);
        textViewPauseLength = (TextView) findViewById(R.id.pauseViewTextViewPauseLength);
        textViewAddOnPauseLength = (TextView) findViewById(R.id.pauseViewTextViewAddOnPauseLength);
        buttonEdit = (Button) findViewById(R.id.pauseViewButtonEdit);
        buttonDelete = (Button) findViewById(R.id.pauseViewButtonDelete);

        buttonEdit.setOnClickListener(new ButtonEditListener());
        buttonDelete.setOnClickListener(new ButtonDeleteListener());

        pauseId = getIntent().getExtras().getLong(ViewPauseActivity.EXTRA_PAUSE_ID);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new GetPauseTask().execute();
    }

    class ButtonEditListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(ViewPauseActivity.this, EditPauseActivity.class);
            intent.putExtra(EditPauseActivity.EXTRA_PAUSE_ID, pauseId);
            startActivity(intent);
        }

    }

    class ButtonDeleteListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            GUIElementCreators.showAlertDialog(ViewPauseActivity.this, getString(R.string.common_deleteConfirmation), new ButtonConfirmDeleteListener());
        }

        class ButtonConfirmDeleteListener implements DialogInterface.OnClickListener {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                new DeletePauseTask().execute();
            }

        }

    }

    class GetPauseTask extends HttpCallTask<Pause> {

        public GetPauseTask() {
            super(ViewPauseActivity.this, "/pause/" + pauseId, GET, FORM_URLENCODED, JSON, null, Pause.class, false, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<Pause> result) {
            Pause pause = result.getResult();

            textViewCaption.setText(pause.getCaption());
            textViewPauseMinute.setText(pause.getPauseMinute() != null ? String.valueOf(pause.getPauseMinute()) : "");
            textViewNumberOfLevels.setText(pause.getNumberOfLevels() != null ? String.valueOf(pause.getNumberOfLevels()) : "");
            textViewPauseLength.setText(String.valueOf(pause.getPauseLength()));
            textViewAddOnPauseLength.setText(pause.getAddOnPauseLength() != null ? String.valueOf(pause.getAddOnPauseLength()) : "");
        }

    }

    class DeletePauseTask extends HttpCallTask<Boolean> {

        public DeletePauseTask() {
            super(ViewPauseActivity.this, "/pause/" + pauseId, DELETE, FORM_URLENCODED, JSON, null, Boolean.class, false, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<Boolean> result) {
            if (result.getError() == null) {
                Toast.makeText(ViewPauseActivity.this, R.string.pause_pauseSuccessfullyDeleted, Toast.LENGTH_LONG).show();
                finish();
            }
        }

    }

}
