package lt.ktu.jonmic.android.model.entities;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import lt.ktu.jonmic.android.model.constants.PrizePoolType;

public class PrizePool extends AbstractEntity {

    private Long id;
    private String caption;
    private PrizePoolType type;
    private BigDecimal rake;
    private List<Prize> prizes = new ArrayList<>();
    private Boolean deleted;
    private Long userId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public PrizePoolType getType() {
        return type;
    }

    public void setType(PrizePoolType type) {
        this.type = type;
    }

    public BigDecimal getRake() {
        return rake;
    }

    public void setRake(BigDecimal rake) {
        this.rake = rake;
    }

    public List<Prize> getPrizes() {
        return prizes;
    }

    public void setPrizes(List<Prize> prizes) {
        this.prizes = prizes;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return caption;
    }

}
