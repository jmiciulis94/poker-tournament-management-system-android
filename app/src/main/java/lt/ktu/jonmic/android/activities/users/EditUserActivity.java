package lt.ktu.jonmic.android.activities.users;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import lt.ktu.jonmic.R;
import lt.ktu.jonmic.android.activities.BaseActivity;
import lt.ktu.jonmic.android.model.common.TaskResult;
import lt.ktu.jonmic.android.model.entities.User;
import lt.ktu.jonmic.android.tasks.HttpCallTask;
import lt.ktu.jonmic.android.utilities.MarshallingUtils;

public class EditUserActivity extends BaseActivity {

    public static final String EXTRA_EDIT_USER = "lt.ktu.jonmic.android.activities.users.EditUserActivity.EXTRA_EDIT_USER";

    private EditText editTextUsername;
    private EditText editTextPassword;
    private EditText editTextRepeatPassword;
    private EditText editTextFirstName;
    private EditText editTextLastName;
    private EditText editTextEmail;
    private Button buttonSave;
    private Long userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_edit);

        editTextUsername = (EditText) findViewById(R.id.userEditEditTextUsername);
        editTextPassword = (EditText) findViewById(R.id.userEditEditTextPassword);
        editTextRepeatPassword = (EditText) findViewById(R.id.userEditEditTextRepeatPassword);
        editTextFirstName = (EditText) findViewById(R.id.userEditEditTextFirstName);
        editTextLastName = (EditText) findViewById(R.id.userEditEditTextLastName);
        editTextEmail = (EditText) findViewById(R.id.userEditEditTextEmail);
        buttonSave = (Button) findViewById(R.id.userEditButtonSave);

        buttonSave.setOnClickListener(new ButtonSaveListener());

        if (getIntent().hasExtra(EditUserActivity.EXTRA_EDIT_USER)) {
            new GetUserTask().execute();
        }
    }

    class ButtonSaveListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            if (!editTextPassword.getText().toString().equals(editTextRepeatPassword.getText().toString())) {
                Toast.makeText(EditUserActivity.this, R.string.user_passwordsDoNotMatch, Toast.LENGTH_LONG).show();
            } else {
                User user = new User();

                user.setId(userId);
                user.setUsername(editTextUsername.getText().toString());
                user.setPassword(editTextPassword.getText().toString());
                user.setFirstName(editTextFirstName.getText().toString());
                user.setLastName(editTextLastName.getText().toString());
                user.setEmail(editTextEmail.getText().toString());

                String json = MarshallingUtils.convertObjectToJsonBytes(user);
                new SaveUserTask(json).execute();
            }
        }

    }

    class GetUserTask extends HttpCallTask<User> {

        public GetUserTask() {
            super(EditUserActivity.this, "/user/", GET, FORM_URLENCODED, JSON, null, User.class, false, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<User> result) {
            User user = result.getResult();

            userId = user.getId();

            editTextUsername.setText(user.getUsername());
            editTextFirstName.setText(user.getFirstName());
            editTextLastName.setText(user.getLastName());
            editTextEmail.setText(user.getEmail());
        }

    }

    class SaveUserTask extends HttpCallTask<User> {

        public SaveUserTask(String data) {
            super(EditUserActivity.this, "/user", PUT, JSON, JSON, data, User.class, false, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<User> result) {
            if (result.getError() == null) {
                Toast.makeText(EditUserActivity.this, R.string.user_userSuccessfullySaved, Toast.LENGTH_LONG).show();
                finish();
            }
        }

    }

}
