package lt.ktu.jonmic.android.utilities;

import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lt.ktu.jonmic.android.model.common.ServerError;
import lt.ktu.jonmic.android.model.common.TaskResult;

public class HttpUtils {

    public static <T> TaskResult<T> doHttpCall(String httpAddress, String httpMethod, String contentType, String acceptType, String data, Class resultClass, boolean isList, String sessionId) throws IOException {
        URL url;
        HttpURLConnection connection = null;

        try {
            url = new URL(httpAddress);
            connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(20000);
            connection.setRequestMethod(httpMethod);
            connection.setRequestProperty("Accept", acceptType);
            connection.setRequestProperty("Content-Type", contentType);
            connection.setRequestProperty("Cookie", sessionId);

            if (httpMethod.equals("PUT") || httpMethod.equals("POST")) {
                if (data != null) {
                    DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream());
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                    writer.write(data);
                    writer.close();
                    outputStream.close();
                }
            }

            if (connection.getResponseCode() == 200) {
                String json = StringUtils.getStringFromInputSream(connection.getInputStream());

                T result;
                if (json.isEmpty()) {
                    result = null;
                } else if (isList) {
                    result = (T) MarshallingUtils.convertJsonToObjectsList(json, resultClass);
                } else {
                    result = (T) MarshallingUtils.convertJsonToObject(json, resultClass);
                }

                String cookie = connection.getHeaderField("Set-Cookie");
                if (cookie != null) {
                    Pattern pattern = Pattern.compile("JSESSIONID=\\w+");
                    Matcher matcher = pattern.matcher(cookie);

                    String jSessionId = null;
                    if (matcher.find()) {
                        jSessionId = matcher.group();
                    }

                    return new TaskResult<T>(result, jSessionId);
                } else {
                    return new TaskResult<T>(result, null);
                }
            } else if (connection.getResponseCode() == 401) {
                throw new SecurityException();
            } else {
                String json = StringUtils.getStringFromInputSream(connection.getErrorStream());
                ServerError result = MarshallingUtils.convertJsonToObject(json, ServerError.class);
                return new TaskResult<T>(result);
            }
        } finally {
            connection.disconnect();
        }
    }

}
