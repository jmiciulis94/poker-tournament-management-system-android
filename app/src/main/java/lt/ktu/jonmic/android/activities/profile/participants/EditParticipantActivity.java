package lt.ktu.jonmic.android.activities.profile.participants;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import lt.ktu.jonmic.R;
import lt.ktu.jonmic.android.activities.BaseActivity;
import lt.ktu.jonmic.android.model.common.TaskResult;
import lt.ktu.jonmic.android.model.entities.Participant;
import lt.ktu.jonmic.android.tasks.HttpCallTask;
import lt.ktu.jonmic.android.utilities.MarshallingUtils;

public class EditParticipantActivity extends BaseActivity {

    public static final String EXTRA_PARTICIPANT_ID = "lt.ktu.jonmic.android.activities.profile.participants.EditParticipantActivity.PARTICIPANT_ID";

    private EditText editTextFirstName;
    private EditText editTextLastName;
    private EditText editTextNickname;
    private Button buttonSave;
    private Long participantId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.participant_edit);

        editTextFirstName = (EditText) findViewById(R.id.participantEditEditTextFirstName);
        editTextLastName = (EditText) findViewById(R.id.participantEditEditTextLastName);
        editTextNickname = (EditText) findViewById(R.id.participantEditEditTextNickname);
        buttonSave = (Button) findViewById(R.id.participantEditButtonSave);

        buttonSave.setOnClickListener(new ButtonSaveListener());

        if (getIntent().hasExtra(EditParticipantActivity.EXTRA_PARTICIPANT_ID)) {
            participantId = getIntent().getExtras().getLong(EditParticipantActivity.EXTRA_PARTICIPANT_ID);
            new GetParticipantTask().execute();
        }
    }

    class ButtonSaveListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            Participant participant = new Participant();

            String firstName = editTextFirstName.getText().toString();
            String lastName = editTextLastName.getText().toString();
            String nickname = editTextNickname.getText().toString();

            participant.setId(participantId);
            participant.setFirstName(firstName);
            participant.setLastName(lastName);
            participant.setNickname(nickname);

            String json = MarshallingUtils.convertObjectToJsonBytes(participant);
            new SaveParticipantTask(json).execute();
        }

    }

    class GetParticipantTask extends HttpCallTask<Participant> {

        public GetParticipantTask() {
            super(EditParticipantActivity.this, "/participant/" + participantId, GET, FORM_URLENCODED, JSON, null, Participant.class, false, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<Participant> result) {
            Participant participant = result.getResult();

            editTextFirstName.setText(participant.getFirstName());
            editTextLastName.setText(participant.getLastName());
            editTextNickname.setText(participant.getNickname());
        }

    }

    class SaveParticipantTask extends HttpCallTask<Participant> {


        public SaveParticipantTask(String data) {
            super(EditParticipantActivity.this, "/participant", PUT, JSON, JSON, data, Participant.class, false, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<Participant> result) {
            if (result.getError() == null) {
                Toast.makeText(EditParticipantActivity.this, R.string.participant_participantSuccessfullySaved, Toast.LENGTH_LONG).show();
                finish();
            }
        }

    }

}
