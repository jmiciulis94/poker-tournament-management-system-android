package lt.ktu.jonmic.android.activities.tournaments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;

import lt.ktu.jonmic.R;
import lt.ktu.jonmic.android.activities.BaseActivity;
import lt.ktu.jonmic.android.activities.settings.SettingsActivity;
import lt.ktu.jonmic.android.gui.GUIElementCreators;
import lt.ktu.jonmic.android.model.common.TaskResult;
import lt.ktu.jonmic.android.model.constants.Action;
import lt.ktu.jonmic.android.model.entities.Tournament;
import lt.ktu.jonmic.android.model.entities.TournamentAction;
import lt.ktu.jonmic.android.tasks.HttpCallTask;
import lt.ktu.jonmic.android.utilities.TournamentUtils;

public class ViewTournamentActivity extends BaseActivity {

    public static final String EXTRA_TOURNAMENT_ID = "lt.ktu.jonmic.android.activities.tournaments.ViewTournamentActivity.TOURNAMENT_ID";

    private TextView textViewId;
    private TextView textViewCaption;
    private TextView textViewPassword;
    private TextView textViewStartDate;
    private TextView textViewFinishDate;
    private TextView textViewLevelTime;
    private TextView textViewBuyIn;
    private TextView textViewReBuy;
    private TextView textViewAddOn;
    private TextView textViewAddOnLevel;
    private TextView textViewChipsAtBeginning;
    private TextView textViewChipsForReBuy;
    private TextView textViewChipsForAddOn;
    private TextView textViewBlinds;
    private TextView textViewPause;
    private TextView textViewPokerChipsSet;
    private TextView textViewPrizePool;
    private LinearLayout linearLayoutParticipants;
    private LinearLayout linearLayoutActions;
    private TableLayout tableLayoutParticipants;
    private TableLayout tableLayoutActions;
    private Button buttonDashboard;
    private Button buttonEdit;
    private Button buttonDelete;
    private Long tournamentId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tournament_view);

        textViewId = (TextView) findViewById(R.id.tournamentViewTextViewId);
        textViewCaption = (TextView) findViewById(R.id.tournamentViewTextViewCaption);
        textViewPassword = (TextView) findViewById(R.id.tournamentViewTextViewPassword);
        textViewStartDate = (TextView) findViewById(R.id.tournamentViewTextViewStartDate);
        textViewFinishDate = (TextView) findViewById(R.id.tournamentViewTextViewFinishDate);
        textViewLevelTime = (TextView) findViewById(R.id.tournamentViewTextViewLevelTime);
        textViewBuyIn = (TextView) findViewById(R.id.tournamentViewTextViewBuyIn);
        textViewReBuy = (TextView) findViewById(R.id.tournamentViewTextViewReBuy);
        textViewAddOn = (TextView) findViewById(R.id.tournamentViewTextViewAddOn);
        textViewAddOnLevel = (TextView) findViewById(R.id.tournamentViewTextViewAddOnLevel);
        textViewChipsAtBeginning = (TextView) findViewById(R.id.tournamentViewTextViewChipsAtBeginning);
        textViewChipsForReBuy = (TextView) findViewById(R.id.tournamentViewTextViewChipsForReBuy);
        textViewChipsForAddOn = (TextView) findViewById(R.id.tournamentViewTextViewChipsForAddOn);
        textViewBlinds = (TextView) findViewById(R.id.tournamentViewTextViewBlinds);
        textViewPause = (TextView) findViewById(R.id.tournamentViewTextViewPause);
        textViewPokerChipsSet = (TextView) findViewById(R.id.tournamentViewTextViewPokerChipsSet);
        textViewPrizePool = (TextView) findViewById(R.id.tournamentViewTextViewPrizePool);
        linearLayoutParticipants = (LinearLayout) findViewById(R.id.tournamentViewLinearLayoutParticipants);
        linearLayoutActions = (LinearLayout) findViewById(R.id.tournamentViewLinearLayoutActions);
        tableLayoutParticipants = (TableLayout) findViewById(R.id.tournamentViewTableLayoutParticipants);
        tableLayoutActions = (TableLayout) findViewById(R.id.tournamentViewTableLayoutActions);
        buttonDashboard = (Button) findViewById(R.id.tournamentViewButtonDashboard);
        buttonEdit = (Button) findViewById(R.id.tournamentViewButtonEdit);
        buttonDelete = (Button) findViewById(R.id.tournamentViewButtonDelete);

        buttonDashboard.setOnClickListener(new ButtonDashboardListener());
        buttonEdit.setOnClickListener(new ButtonEditListener());
        buttonDelete.setOnClickListener(new ButtonDeleteListener());

        tournamentId = getIntent().getExtras().getLong(ViewTournamentActivity.EXTRA_TOURNAMENT_ID);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new GetTournamentTask().execute();
    }

    class ButtonDashboardListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(ViewTournamentActivity.this, TournamentActivity.class);
            intent.putExtra(TournamentActivity.EXTRA_TOURNAMENT_ID, tournamentId);
            startActivity(intent);
        }

    }

    class ButtonEditListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(ViewTournamentActivity.this, EditTournamentActivity.class);
            intent.putExtra(EditTournamentActivity.EXTRA_TOURNAMENT_ID, tournamentId);
            startActivity(intent);
        }

    }

    class ButtonDeleteListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            GUIElementCreators.showAlertDialog(ViewTournamentActivity.this, getString(R.string.common_deleteConfirmation), new ButtonConfirmDeleteListener());
        }

        class ButtonConfirmDeleteListener implements DialogInterface.OnClickListener {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                new DeleteTournamentTask().execute();
            }

        }

    }

    class GetTournamentTask extends HttpCallTask<Tournament> {

        public GetTournamentTask() {
            super(ViewTournamentActivity.this, "/tournament/" + tournamentId, GET, FORM_URLENCODED, JSON, null, Tournament.class, false, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<Tournament> result) {
            Tournament tournament = result.getResult();

            Date tournamentStartDate = TournamentUtils.getTournamentStartDate(tournament);
            Date tournamentFinishDate = TournamentUtils.getTournamentFinishDate(tournament);

            textViewId.setText(String.valueOf(tournament.getId()));
            textViewCaption.setText(tournament.getCaption());
            textViewPassword.setText(tournament.getPassword());
            textViewStartDate.setText(tournamentStartDate != null ? SettingsActivity.getDateTimeFormat(ViewTournamentActivity.this).format(tournamentStartDate) : "");
            textViewFinishDate.setText(tournamentFinishDate != null ? SettingsActivity.getDateTimeFormat(ViewTournamentActivity.this).format(tournamentFinishDate) : "");
            textViewLevelTime.setText(String.valueOf(tournament.getLevelTime()));
            textViewBuyIn.setText(tournament.getBuyIn() != null ? tournament.getBuyIn().toPlainString() + " " + SettingsActivity.getCurrency(ViewTournamentActivity.this) : "");
            textViewReBuy.setText(tournament.getReBuy() != null ? tournament.getReBuy().toPlainString() + " " + SettingsActivity.getCurrency(ViewTournamentActivity.this) : "");
            textViewAddOn.setText(tournament.getAddOn() != null ? tournament.getAddOn().toPlainString() + " " + SettingsActivity.getCurrency(ViewTournamentActivity.this) : "");
            textViewAddOnLevel.setText(tournament.getAddOnLevel() != null ? String.valueOf(tournament.getAddOnLevel()) : "");
            textViewChipsAtBeginning.setText(tournament.getChipsAtBeginning() != null ? tournament.getChipsAtBeginning().toPlainString() : "");
            textViewChipsForReBuy.setText(tournament.getChipsForReBuy() != null ? tournament.getChipsForReBuy().toPlainString() : "");
            textViewChipsForAddOn.setText(tournament.getChipsForAddOn() != null ? tournament.getChipsForAddOn().toPlainString() : "");
            textViewBlinds.setText(tournament.getBlinds().getCaption());
            textViewPause.setText(tournament.getPause() != null ? tournament.getPause().getCaption() : "");
            textViewPokerChipsSet.setText(tournament.getPokerChipsSet() != null ? tournament.getPokerChipsSet().getCaption() : "");
            textViewPrizePool.setText(tournament.getPrizePool() != null ? tournament.getPrizePool().getCaption() : "");
            tableLayoutParticipants.removeViews(1, tableLayoutParticipants.getChildCount() - 1);
            tableLayoutActions.removeViews(1, tableLayoutActions.getChildCount() - 1);

            for (int i = 0; i < tournament.getParticipants().size(); i++) {
                LayoutInflater inflater = (LayoutInflater) ViewTournamentActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                TableRow row = (TableRow) inflater.inflate(R.layout.tournament_view_participant_row, null);

                ((TextView) row.findViewById(R.id.tournamentViewRowTextViewNo)).setText(String.valueOf(i + 1));
                ((TextView) row.findViewById(R.id.tournamentViewRowTextViewParticipant)).setText(tournament.getParticipants().get(i).toString());

                tableLayoutParticipants.addView(row);
            }

            for (int i = 0; i < tournament.getTournamentActions().size(); i++) {
                LayoutInflater inflater = (LayoutInflater) ViewTournamentActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                TableRow row = (TableRow) inflater.inflate(R.layout.tournament_view_action_row, null);

                String time = SettingsActivity.getDateTimeFormat(ViewTournamentActivity.this).format(tournament.getTournamentActions().get(i).getDate());
                TournamentAction tournamentAction = tournament.getTournamentActions().get(i);

                String details = "";
                if (tournamentAction.getAction() == Action.PARTICIPANT_DROP || tournamentAction.getAction() == Action.PARTICIPANT_REBUY || tournamentAction.getAction() == Action.PARTICIPANT_ADDON) {
                    details = TournamentUtils.getParticipant(tournament, Long.valueOf(tournamentAction.getDetails())).toString();
                } else {
                    details = tournamentAction.getDetails();
                }

                ((TextView) row.findViewById(R.id.tournamentViewRowTextViewTime)).setText(time);
                ((TextView) row.findViewById(R.id.tournamentViewRowTextViewAction)).setText(Action.getActionName(ViewTournamentActivity.this, tournamentAction.getAction(), details));

                tableLayoutActions.addView(row);
            }

            if (tournament.getParticipants().isEmpty()) {
                linearLayoutParticipants.setVisibility(View.GONE);
            }

            if (tournament.getTournamentActions().isEmpty()) {
                linearLayoutActions.setVisibility(View.GONE);
            }

            if (tournamentFinishDate != null) {
                buttonDashboard.setVisibility(View.GONE);
            }
        }

    }

    class DeleteTournamentTask extends HttpCallTask<Boolean> {

        public DeleteTournamentTask() {
            super(ViewTournamentActivity.this, "/tournament/" + tournamentId, DELETE, FORM_URLENCODED, JSON, null, Boolean.class, false, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<Boolean> result) {
            if (result.getError() == null) {
                Toast.makeText(ViewTournamentActivity.this, R.string.tournament_tournamentSuccessfullyDeleted, Toast.LENGTH_LONG).show();
                finish();
            }
        }

    }

}
