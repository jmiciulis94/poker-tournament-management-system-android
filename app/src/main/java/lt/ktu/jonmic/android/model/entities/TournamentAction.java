package lt.ktu.jonmic.android.model.entities;

import java.util.Date;
import lt.ktu.jonmic.android.model.constants.Action;

public class TournamentAction extends AbstractEntity {

    private Long id;
    private User user;
    private Action action;
    private String details;
    private Date date;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
