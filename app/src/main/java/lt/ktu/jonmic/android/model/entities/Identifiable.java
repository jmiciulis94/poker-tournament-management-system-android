package lt.ktu.jonmic.android.model.entities;

public interface Identifiable {

    Long getId();

    void setId(Long id);

}
