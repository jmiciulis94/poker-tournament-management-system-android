package lt.ktu.jonmic.android.activities.profile.prizes;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import java.util.List;
import lt.ktu.jonmic.R;
import lt.ktu.jonmic.android.activities.BaseActivity;
import lt.ktu.jonmic.android.gui.GUIElementCreators;
import lt.ktu.jonmic.android.model.common.TaskResult;
import lt.ktu.jonmic.android.model.entities.PrizePool;
import lt.ktu.jonmic.android.tasks.HttpCallTask;

public class PrizePoolsListActivity extends BaseActivity {

    private ListView listViewPrizePools;
    private Button buttonNew;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.prize_pools_list);

        listViewPrizePools = (ListView) findViewById(R.id.prizePoolsListListViewPrizePools);
        buttonNew = (Button) findViewById(R.id.prizePoolsListButtonNew);

        listViewPrizePools.setOnItemClickListener(new ListViewPrizePoolsListener());
        buttonNew.setOnClickListener(new ButtonNewListener());

        registerForContextMenu(listViewPrizePools);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, view, menuInfo);

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        PrizePool prizePool = (PrizePool) ((ListView) view).getAdapter().getItem(info.position);
        menu.setHeaderTitle(prizePool.getCaption());

        menu.add(Menu.NONE, prizePool.getId().intValue(), 1, getString(R.string.common_view));
        menu.add(Menu.NONE, prizePool.getId().intValue(), 2, getString(R.string.common_edit));
        menu.add(Menu.NONE, prizePool.getId().intValue(), 3, getString(R.string.common_delete));
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getTitle().equals(getString(R.string.common_view))) {
            Intent intent = new Intent(this, ViewPrizePoolActivity.class);
            intent.putExtra(ViewPrizePoolActivity.EXTRA_PRIZE_POOL_ID, (long) item.getItemId());
            startActivity(intent);
        } else if (item.getTitle().equals(getString(R.string.common_edit))) {
            Intent intent = new Intent(this, EditPrizePoolActivity.class);
            intent.putExtra(EditPrizePoolActivity.EXTRA_PRIZE_POOL_ID, (long) item.getItemId());
            startActivity(intent);
        } else if (item.getTitle().equals(getString(R.string.common_delete))) {
            GUIElementCreators.showAlertDialog(this, getString(R.string.common_deleteConfirmation), new ButtonConfirmDeleteListener((long) item.getItemId()));
        }

        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        new GetPrizePoolsTask().execute();
    }

    class ListViewPrizePoolsListener implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            PrizePool prizePool = (PrizePool) parent.getItemAtPosition(position);
            Intent intent = new Intent(PrizePoolsListActivity.this, ViewPrizePoolActivity.class);
            intent.putExtra(ViewPrizePoolActivity.EXTRA_PRIZE_POOL_ID, prizePool.getId());
            startActivity(intent);
        }

    }

    class ButtonConfirmDeleteListener implements DialogInterface.OnClickListener {

        private Long prizePoolId;

        public ButtonConfirmDeleteListener(Long prizePoolId) {
            this.prizePoolId = prizePoolId;
        }

        @Override
        public void onClick(DialogInterface dialog, int which) {
            new DeletePrizePoolTask(prizePoolId).execute();
            new GetPrizePoolsTask().execute();
        }

    }

    class ButtonNewListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            startActivity(new Intent(PrizePoolsListActivity.this, EditPrizePoolActivity.class));
        }

    }

    class GetPrizePoolsTask extends HttpCallTask<List<PrizePool>> {

        public GetPrizePoolsTask() {
            super(PrizePoolsListActivity.this, "/prize-pool", GET, FORM_URLENCODED, JSON, null, PrizePool.class, true, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<List<PrizePool>> result) {
            listViewPrizePools.setAdapter(new ArrayAdapter<>(PrizePoolsListActivity.this, android.R.layout.simple_list_item_1, result.getResult()));
        }

    }

    class DeletePrizePoolTask extends HttpCallTask<Boolean> {

        public DeletePrizePoolTask(long prizePoolId) {
            super(PrizePoolsListActivity.this, "/prize-pool/" + prizePoolId, DELETE, FORM_URLENCODED, JSON, null, Boolean.class, false, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<Boolean> result) {
            if (result.getError() == null) {
                Toast.makeText(PrizePoolsListActivity.this, R.string.prizePool_prizePoolSuccessfullyDeleted, Toast.LENGTH_LONG).show();
            }
        }

    }

}
