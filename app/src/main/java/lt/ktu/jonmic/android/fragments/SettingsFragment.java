package lt.ktu.jonmic.android.fragments;

import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import lt.ktu.jonmic.R;

public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    public static final String KEY_LOGIN_USERNAME = "LOGIN.USERNAME";
    public static final String KEY_LOGIN_PASSWORD = "LOGIN.PASSWORD";
    public static final String KEY_LOGIN_REMEMBER_ME = "LOGIN.REMEMBER_ME";
    public static final String KEY_INTERNATIONALIZATION_LANGUAGE = "INTERNATIONALIZATION.LANGUAGE";
    public static final String KEY_INTERNATIONALIZATION_DATE_FORMAT = "INTERNATIONALIZATION.DATE_FORMAT";
    public static final String KEY_INTERNATIONALIZATION_CURRENCY = "INTERNATIONALIZATION.CURRENCY";
    public static final String KEY_DASHBOARD_MAX_NUMBER_OF_PARTICIPANTS = "DASHBOARD.MAX_NUMBER_OF_PARTICIPANTS";
    public static final String KEY_DASHBOARD_MAX_NUMBER_OF_PRIZES = "DASHBOARD.MAX_NUMBER_OF_PRIZES";
    public static final String KEY_DASHBOARD_SHOW_TIME_FOR_REBUYS = "DASHBOARD.SHOW_TIME_FOR_REBUYS";
    public static final String KEY_DASHBOARD_SHOW_TIME_UNTIL_PAUSE = "DASHBOARD.SHOW_TIME_UNTIL_PAUSE";
    public static final String KEY_DASHBOARD_SHOW_TOURNAMENT_CAPTION = "DASHBOARD.SHOW_TOURNAMENT_CAPTION";
    public static final String KEY_DASHBOARD_SHOW_TOURNAMENT_START_TIME = "DASHBOARD.SHOW_TOURNAMENT_START_TIME";
    public static final String KEY_DASHBOARD_SHOW_PRICES = "DASHBOARD.SHOW_PRICES";
    public static final String KEY_DASHBOARD_SHOW_LEVEL_TIME = "DASHBOARD.SHOW_LEVEL_TIME";
    public static final String KEY_DASHBOARD_SHOW_NUMBER_OF_PARTICIPANTS = "DASHBOARD.SHOW_NUMBER_OF_PARTICIPANTS";
    public static final String KEY_DASHBOARD_SHOW_AVERAGE_STACK = "DASHBOARD.SHOW_AVERAGE_STACK";
    public static final String KEY_DASHBOARD_SHOW_TOTAL_REBUYS_AND_ADDONS = "DASHBOARD.SHOW_TOTAL_REBUYS_AND_ADDONS";
    public static final String KEY_DASHBOARD_SHOW_CHIP_VALUES = "DASHBOARD.SHOW_CHIP_VALUES";
    public static final String KEY_DASHBOARD_SHOW_PARTICIPANTS = "DASHBOARD.SHOW_PARTICIPANTS";
    public static final String KEY_DASHBOARD_SHOW_PRIZE_POOL = "DASHBOARD.SHOW_PRIZE_POOL";
    public static final String KEY_DASHBOARD_SHOW_BUTTONS = "DASHBOARD.SHOW_BUTTONS";
    public static final String KEY_ABOUT_VERSION = "ABOUT.VERSION";
    public static final String VALUE_DATE_FORMAT_US = "01/25/2014";
    public static final String VALUE_DATE_FORMAT_RU = "25/01/2014";
    public static final String VALUE_DATE_FORMAT_EU = "2014-01-25";

    private SharedPreferences sharedPreferences;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        setPreferenceSummary(SettingsFragment.KEY_INTERNATIONALIZATION_LANGUAGE);
        setPreferenceSummary(SettingsFragment.KEY_INTERNATIONALIZATION_DATE_FORMAT);
        setPreferenceSummary(SettingsFragment.KEY_INTERNATIONALIZATION_CURRENCY);
        setPreferenceSummary(SettingsFragment.KEY_DASHBOARD_MAX_NUMBER_OF_PARTICIPANTS);
        setPreferenceSummary(SettingsFragment.KEY_DASHBOARD_MAX_NUMBER_OF_PRIZES);

        try {
            Preference versionPreference = findPreference(KEY_ABOUT_VERSION);
            versionPreference.setSummary(getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        super.onPause();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(SettingsFragment.KEY_INTERNATIONALIZATION_LANGUAGE)) {
            setPreferenceSummary(SettingsFragment.KEY_INTERNATIONALIZATION_LANGUAGE);
        } else if (key.equals(SettingsFragment.KEY_INTERNATIONALIZATION_DATE_FORMAT)) {
            setPreferenceSummary(SettingsFragment.KEY_INTERNATIONALIZATION_DATE_FORMAT);
        } else if (key.equals(SettingsFragment.KEY_INTERNATIONALIZATION_CURRENCY)) {
            setPreferenceSummary(SettingsFragment.KEY_INTERNATIONALIZATION_CURRENCY);
        } else if (key.equals(SettingsFragment.KEY_DASHBOARD_MAX_NUMBER_OF_PARTICIPANTS)) {
            removeStartingZeros(SettingsFragment.KEY_DASHBOARD_MAX_NUMBER_OF_PARTICIPANTS);
            setPreferenceSummary(SettingsFragment.KEY_DASHBOARD_MAX_NUMBER_OF_PARTICIPANTS);
        } else if (key.equals(SettingsFragment.KEY_DASHBOARD_MAX_NUMBER_OF_PRIZES)) {
            removeStartingZeros(SettingsFragment.KEY_DASHBOARD_MAX_NUMBER_OF_PARTICIPANTS);
            setPreferenceSummary(SettingsFragment.KEY_DASHBOARD_MAX_NUMBER_OF_PRIZES);
        }
    }

    private void setPreferenceSummary(String key) {
        String value = sharedPreferences.getString(key, "");
        Preference preference = findPreference(key);
        preference.setSummary(String.format("%s: %s", getString(R.string.preferences_current), value));
    }

    private void removeStartingZeros(String key) {
        String value = sharedPreferences.getString(key, "");
        if (value.startsWith("0")) {
            while (value.startsWith("0") && value.length() > 1) {
                value = value.substring(1);
            }

            EditTextPreference preference = (EditTextPreference) findPreference(key);
            preference.setText(value);
        }
    }

}
