package lt.ktu.jonmic.android.activities.profile.chips;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import java.util.List;
import lt.ktu.jonmic.R;
import lt.ktu.jonmic.android.activities.BaseActivity;
import lt.ktu.jonmic.android.gui.GUIElementCreators;
import lt.ktu.jonmic.android.model.common.TaskResult;
import lt.ktu.jonmic.android.model.entities.PokerChipsSet;
import lt.ktu.jonmic.android.tasks.HttpCallTask;

public class PokerChipsSetsListActivity extends BaseActivity {

    private ListView listViewPokerChipsSets;
    private Button buttonNew;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.poker_chips_sets_list);

        listViewPokerChipsSets = (ListView) findViewById(R.id.pokerChipsSetsListListViewPokerChipsSets);
        buttonNew = (Button) findViewById(R.id.pokerChipsSetsListButtonNew);

        listViewPokerChipsSets.setOnItemClickListener(new ListViewPokerChipsSetsListener());
        buttonNew.setOnClickListener(new ButtonNewListener());

        registerForContextMenu(listViewPokerChipsSets);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, view, menuInfo);

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        PokerChipsSet pokerChipsSet = (PokerChipsSet) ((ListView) view).getAdapter().getItem(info.position);
        menu.setHeaderTitle(pokerChipsSet.getCaption());

        menu.add(Menu.NONE, pokerChipsSet.getId().intValue(), 1, getString(R.string.common_view));
        menu.add(Menu.NONE, pokerChipsSet.getId().intValue(), 2, getString(R.string.common_edit));
        menu.add(Menu.NONE, pokerChipsSet.getId().intValue(), 3, getString(R.string.common_delete));
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getTitle().equals(getString(R.string.common_view))) {
            Intent intent = new Intent(this, ViewPokerChipsSetActivity.class);
            intent.putExtra(ViewPokerChipsSetActivity.EXTRA_POKER_CHIPS_SET_ID, (long) item.getItemId());
            startActivity(intent);
        } else if (item.getTitle().equals(getString(R.string.common_edit))) {
            Intent intent = new Intent(this, EditPokerChipsSetActivity.class);
            intent.putExtra(EditPokerChipsSetActivity.EXTRA_POKER_CHIPS_SET_ID, (long) item.getItemId());
            startActivity(intent);
        } else if (item.getTitle().equals(getString(R.string.common_delete))) {
            GUIElementCreators.showAlertDialog(this, getString(R.string.common_deleteConfirmation), new ButtonConfirmDeleteListener((long) item.getItemId()));
        }

        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        new GetPokerChipsSetsTask().execute();
    }

    class ListViewPokerChipsSetsListener implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            PokerChipsSet pokerChipsSet = (PokerChipsSet) parent.getItemAtPosition(position);
            Intent intent = new Intent(PokerChipsSetsListActivity.this, ViewPokerChipsSetActivity.class);
            intent.putExtra(ViewPokerChipsSetActivity.EXTRA_POKER_CHIPS_SET_ID, pokerChipsSet.getId());
            startActivity(intent);
        }

    }

    class ButtonConfirmDeleteListener implements DialogInterface.OnClickListener {

        private Long pokerChipsSetId;

        public ButtonConfirmDeleteListener(Long pokerChipsSetId) {
            this.pokerChipsSetId = pokerChipsSetId;
        }

        @Override
        public void onClick(DialogInterface dialog, int which) {
            new DeletePokerChipsSetTask(pokerChipsSetId).execute();
            new GetPokerChipsSetsTask().execute();
        }

    }

    class ButtonNewListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            startActivity(new Intent(PokerChipsSetsListActivity.this, EditPokerChipsSetActivity.class));
        }

    }

    class GetPokerChipsSetsTask extends HttpCallTask<List<PokerChipsSet>> {

        public GetPokerChipsSetsTask() {
            super(PokerChipsSetsListActivity.this, "/poker-chips-set", GET, FORM_URLENCODED, JSON, null, PokerChipsSet.class, true, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<List<PokerChipsSet>> result) {
            listViewPokerChipsSets.setAdapter(new ArrayAdapter<>(PokerChipsSetsListActivity.this, android.R.layout.simple_list_item_1, result.getResult()));
        }

    }

    class DeletePokerChipsSetTask extends HttpCallTask<Boolean> {

        public DeletePokerChipsSetTask(long pokerChipsSetId) {
            super(PokerChipsSetsListActivity.this, "/poker-chips-set/" + pokerChipsSetId, DELETE, FORM_URLENCODED, JSON, null, Boolean.class, false, true);
        }

        @Override
        protected void postExecuteActions(TaskResult<Boolean> result) {
            if (result.getError() == null) {
                Toast.makeText(PokerChipsSetsListActivity.this, R.string.pokerChipsSet_pokerChipsSetSuccessfullyDeleted, Toast.LENGTH_LONG).show();
            }
        }

    }

}
